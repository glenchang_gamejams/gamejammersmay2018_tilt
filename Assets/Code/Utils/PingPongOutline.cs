﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PingPongOutline : MonoBehaviour
{
    public float m_outlineMinX = 1;
    public float m_outlineMinY = -1;

    public float m_outlineMaxX = 3;
    public float m_outlineMaxY = -3;

    public float m_frequency = 1f;
    public float m_offset = 1f;
    public AnimationCurve m_transition = null;

    private Outline m_outline = null;

    private void Start()
    {
        m_outline = GetComponent<Outline>();
    }

    // Update is called once per frame
    void Update ()
    {

        m_outline.effectDistance = new Vector2(m_outlineMinX + m_outlineMaxX * m_transition.Evaluate(Mathf.PingPong(Time.time * m_frequency + m_offset, 1)),
                                    m_outlineMinY + m_outlineMaxY * m_transition.Evaluate(Mathf.PingPong(Time.time * m_frequency + m_offset, 1)));
    }
}
