﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailTile : Tile
{
    override public void OnExplosion(GameState caller)
    {
        base.OnExplosion(caller);
        EventManager.Fire(new KilledGrandmaEvent());
    }
}
