﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XBomb : Bomb
{

    // Use this for initialization
    void Start()
    {
        bombType = BombType.X;
        debugColor = Color.blue;
    }

}
