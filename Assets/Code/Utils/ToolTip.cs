﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour
{
    public float DelayInSec = 2f;
    private Text m_text = null;

    private void Awake()
    {
        m_text = GetComponent<Text>();
        EventManager.AddListener<ToolTipEvent>(OnToolTip);
        EventManager.AddListener<ToolTipOffEvent>(OnToolTipOff);
        m_text.enabled = false;
    }

    private void OnDestroy()
    {
        EventManager.RemoveListener<ToolTipEvent>(OnToolTip);
        EventManager.RemoveListener<ToolTipOffEvent>(OnToolTipOff);
    }

    public IEnumerator ShowMessageRoutine(string message)
    {
        m_text.enabled = true;
        m_text.text = message;
        yield return new WaitForSeconds(DelayInSec);
        m_text.enabled = false;
    }

    public void OnToolTip(ToolTipEvent evt)
    {
        StopAllCoroutines();
        StartCoroutine(ShowMessageRoutine(evt.Message));
    }

    public void OnToolTipOff(ToolTipOffEvent evt)
    {
        StopAllCoroutines();
        m_text.enabled = false;
    }
}
