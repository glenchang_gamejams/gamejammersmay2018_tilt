﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelSelectButtonScript : MonoBehaviour, IPointerClickHandler {

    public int m_levelIndex;
    public Image m_star;

    public LevelSelectButtonScript()
    {
        m_levelIndex = -1;
    }

    public virtual void OnPointerClick(PointerEventData pointerEventData)
    {
        if (m_levelIndex >= 0)
            GameManager.Instance.LoadLevel(m_levelIndex);
    }
}
