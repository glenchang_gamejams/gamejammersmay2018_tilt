﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextOverlay : MonoBehaviour
{
    public Text m_text = null;

	// Use this for initialization
	void Start ()
    {
        if (m_text == null)
            m_text = GetComponentInChildren<Text>();
	}

    public void SetColor(Color color)
    {
        m_text.color = color;
    }

    public void SetText(string text)
    {
        m_text.text = text;
    }
}
