﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BoardDatabase : MonoBehaviour
{
    public static BoardDatabase Instance = null;
    private const char c_cellDelimiter = ',';
    private BoardData[] m_boardData = null;
    public GameObject[] m_backgrounds = null;

    public int LevelCount { get { return m_boardData.Length; } }

    [SerializeField]
    private string[] m_boardPaths = null;

    [Serializable]
    public class BoardData
    {
        public string[] bombTypes;
        public string[][] tiles;
        public int boardWidth;
        public int boardHeight;
    }

    public int GetBoardCount()
    {
        return m_boardPaths.Length;
    }

    public string GetBoardPath(int boardIndex)
    {
        return (boardIndex < 0 || boardIndex >= m_boardPaths.Length) ? "" : m_boardPaths[boardIndex];
    }

    public string GetBoardReadableName(int boardIndex)
    {
        if (boardIndex < 0 || boardIndex >= m_boardPaths.Length)
            return "";

        string name = m_boardPaths[boardIndex]; 
        int sep = Math.Max(name.LastIndexOf('/'), name.LastIndexOf('\\'));
        if (sep > 0)
            name = name.Substring(sep + 1);

        if (name.StartsWith("level_"))
            name = name.Substring(6);

        return name.Replace('_', ' ');
    }

    public BoardData GetBoard(int index)
    {
        if (index >= m_boardData.Length)
        {
            Debug.LogFormat("No board at invalid index {0}", index);
            return null;
        }

        return m_boardData[index];
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        List<BoardData> boardData = new List<BoardData>();
        for (int i = 0; i < m_boardPaths.Length; i++)
        {
            TextAsset fileText = (TextAsset)Resources.Load("boards/".ConcatString(m_boardPaths[i]), typeof(TextAsset));
            string[] lines = fileText.text.Split('\n');
            //string[] lines = File.ReadAllLines(m_boardPaths[i]);

            if (lines.Length < 2)
            {
                Debug.LogFormat("Failed to create Board, not enough data provided for {}", m_boardPaths[i]);
                continue;
            }

            // Get Bomb Types
            var board = new BoardData();
            {
                string[] bombTypes = lines[0].Split(c_cellDelimiter);
                int dstIndex = 0;
                for (int j = 0; j < bombTypes.Length; j++)
                {
                    string tmp = bombTypes[j].Trim();
                    if (tmp.Length > 0)
                        bombTypes[dstIndex++] = tmp;
                }

                board.bombTypes = new string[dstIndex];
                for (int j = 0; j < dstIndex; j++)
                    board.bombTypes[j] = bombTypes[j];
            }

            // Get Tiles
            List<string[]> tileLines = new List<string[]>();
            for (int x = 1; x < lines.Length; x++)
            {
                var split = lines[x].Split(c_cellDelimiter);
                // Skip empty lines
                if (split.Length <= 1)
                    continue;
                tileLines.Add(split);
            }
            board.tiles = tileLines.ToArray();
            board.boardHeight = tileLines.Count;
            board.boardWidth = tileLines[0].Length;
            boardData.Add(board);
        }
        m_boardData = boardData.ToArray();
    }
}