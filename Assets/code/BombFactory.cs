﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombFactory : MonoBehaviour {

    public static BombFactory Instance = null;
    private const char c_cellDelimiter = ',';

    public SurroundBomb m_surroundBomb = null;
    public CrossBomb m_crossBomb = null;
    public XBomb m_xBomb = null;
    public PushBomb m_pushBomb = null;

    private Bomb CreateBombType<T>(T prefab, string bombStr) where T : Bomb
    {
        if (prefab == null)
            return null;

        var bomb = Instantiate<T>(prefab);
        return bomb;
    }

    private Bomb CreateSurroundBomb(string bombType)
    {
        var bomb = CreateBombType<SurroundBomb>(m_surroundBomb, bombType);
        return bomb;
    }

    private Bomb CreateCrossBomb(string bombType)
    {
        var bomb = CreateBombType<CrossBomb>(m_crossBomb, bombType);
        return bomb;
    }

    private Bomb CreateXBomb(string bombType)
    {
        var bomb = CreateBombType<XBomb>(m_xBomb, bombType);
        return bomb;
    }

    private Bomb CreatePushBomb(string bombType)
    {
        var bomb = CreateBombType<PushBomb>(m_pushBomb, bombType);
        return bomb;
    }

    public Bomb CreateBomb(string bombType, List<Bomb> m_bombs)
    {
        Bomb bomb = null;
        switch (bombType[0])
        {
            case 'B':
                if (bombType.Substring(1) != "0")
                {
                    int numBombs = 0;
                    if (int.TryParse(bombType.Substring(1),out numBombs))
                    {
                        for (int i = 0; i < numBombs; i++)
                        {
                            bomb = CreateSurroundBomb(bombType);
                            if (bomb != null)
                                m_bombs.Add(bomb);
                        }
                    }
                }
                break;
            case '+':
                if (bombType.Substring(1) != "0")
                {
                    int numBombs = 0;
                    if (int.TryParse(bombType.Substring(1), out numBombs))
                    {
                        for (int i = 0; i < numBombs; i++)
                        {
                            bomb = CreateCrossBomb(bombType);
                            if (bomb != null)
                                m_bombs.Add(bomb);
                        }
                    }
                }
                break;
            case 'X':
                if (bombType.Substring(1) != "0")
                {
                    int numBombs = 0;
                    if (int.TryParse(bombType.Substring(1), out numBombs))
                    {
                        for (int i = 0; i < numBombs; i++)
                        {
                            bomb = CreateXBomb(bombType);
                            if (bomb != null)
                                m_bombs.Add(bomb);
                        }
                    }
                }
                break;
            case '>':
                if (bombType.Substring(1) != "0")
                {
                    int numBombs = 0;
                    if (int.TryParse(bombType.Substring(1), out numBombs))
                    {
                        for (int i = 0; i < numBombs; i++)
                        {
                            bomb = CreatePushBomb(bombType);
                            if (bomb != null)
                                m_bombs.Add(bomb);
                        }
                    }
                }
                break;
        }

        return bomb;
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
    }
}