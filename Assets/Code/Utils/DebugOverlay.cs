﻿using UnityEngine;
using System.Collections.Generic;

public class DebugOverlay : MonoBehaviour
{
    private static Dictionary<string, string> m_messages = new Dictionary<string, string>();

    private GUIStyle m_style = new GUIStyle();
    private int m_fontSize = 40;

    // Instance
    private static DebugOverlay m_instance = null;

    public static void Message(object key = null, object value = null)
    {
        if (m_instance == null)
        {
            GameObject newObj = new GameObject("DebugOverlay");
            m_instance = newObj.AddComponent<DebugOverlay>();
        }

        m_messages[key.ToString()] = value.ToString();
    }

    public static void ClearMessages()
    {
        m_messages.Clear();
    }

    public void Clear()
    {
        m_messages.Clear();
    }

    private void Awake()
    {
        m_style.fontSize = m_fontSize;
        m_style.normal.textColor = Color.cyan;
    }

#if (DEBUG || UNITY_EDITOR)
    private void OnGUI()
    {
        int linePos = 10;

        var enumerator = m_messages.GetEnumerator();
        while (enumerator.MoveNext())
        {
            GUI.Label(new Rect(0, linePos, 300, m_fontSize), string.Format("{0} : {1}", enumerator.Current.Key, enumerator.Current.Value), m_style);
            linePos += m_fontSize;
        }
    }
#endif
}
