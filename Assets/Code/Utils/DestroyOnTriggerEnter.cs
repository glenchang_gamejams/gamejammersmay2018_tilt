﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTriggerEnter : MonoBehaviour
{
    [SerializeField]
    float m_delay = 1f;

    void OnTriggerEnter()
    {
        Destroy(gameObject, m_delay);
    }
}
