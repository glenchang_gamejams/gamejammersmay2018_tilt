﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalExplosiveBlockTile : Tile
{
    private bool isExploding = false;
    private int explodeTick = -1;
    private int distance = 1;

    //If a Cross Bomb is blocked, these are true:
    private bool blockedRight = false;
    private bool blockedLeft = false;

    override public void Tick(GameState caller)
    {
        if (!isExploding && caller.GetTickNumber() == explodeTick)
        {
            //Adding sound of bomb exploding
            // Get audiolist
            m_audioList = GetComponent<AudioList>();
            if (m_audioList != null)
                m_audioList.Play();

            isExploding = true;

            //Check the surrounding tiles to see if any tile is blocking an explosion
            if (caller.IsBlockedTile(x + 1, y, this.gameObject))
                blockedRight = true;
            if (caller.IsBlockedTile(x - 1, y, this.gameObject))
                blockedLeft = true;

            //Explode adjacent tiles.
            //Then on the next tick, explode the tiles two spaces away. (See Tick function)
            caller.ExplodeTile(x + 1, y, gameObject);
            caller.ExplodeTile(x - 1, y, gameObject);

            IsProcessing = true;
        }
        else if (isExploding)
        {
            if (blockedLeft && blockedRight)
                IsDestroyed = true; //The bomb is subsequently destroyed
            else
            {
                distance++;

                //Adding sound of bomb exploding
                // Get audiolist
                m_audioList = GetComponent<AudioList>();
                if (m_audioList != null)
                    m_audioList.Play();
                
                if (!blockedRight)
                    caller.ExplodeTile(x + distance, y, gameObject);
                if (!blockedLeft)
                    caller.ExplodeTile(x - distance, y, gameObject);

                //Check the surrounding tiles to see if any tile is blocking an explosion
                if (!blockedRight && caller.IsBlockedTile(x + distance, y, this.gameObject))
                    blockedRight = true;
                if (!blockedLeft && caller.IsBlockedTile(x - distance, y, this.gameObject))
                    blockedLeft = true;
            }

        }
        base.Tick(caller);
    }

    override public void OnExplosion(GameState caller)
    {
        base.OnExplosion(caller);
        if (explodeTick == -1)
        {
            explodeTick = caller.GetTickNumber() + 1;
        }
    }
}
