﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PingPongTextColors : MonoBehaviour
{
    public Color m_color1 = Color.white;
    public Color m_color2 = Color.red;

    private Text m_text = null;

    public float m_frequency = 1f;
    public float m_offset = 1f;
    public AnimationCurve m_transition = null;


    private void Start()
    {
        m_text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update ()
    {
        var color1 = Color.Lerp(m_color1, m_color2, m_transition.Evaluate(Mathf.PingPong(Time.time * m_frequency + m_offset, 1f)));
        if (m_text != null)
            m_text.color = color1;
	}
}
