﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalExplosiveBlockTile : Tile
{
    private bool isExploding = false;
    private int explodeTick = -1;
    private int distance = 1;

    //If a Cross Bomb is blocked, these are true:
    private bool blockedAbove = false;
    private bool blockedBelow = false;

    override public void Tick(GameState caller)
    {
        if (!isExploding && caller.GetTickNumber() == explodeTick)
        {
            //Adding sound of bomb exploding
            // Get audiolist
            m_audioList = GetComponent<AudioList>();
            if (m_audioList != null)
                m_audioList.Play();

            isExploding = true;

            //Check the surrounding tiles to see if any tile is blocking an explosion
            if (caller.IsBlockedTile(x, y + 1, this.gameObject))
                blockedAbove = true;
            if (caller.IsBlockedTile(x, y - 1, this.gameObject))
                blockedBelow = true;

            //Explode adjacent tiles.
            //Then on the next tick, explode the tiles two spaces away. (See Tick function)
            caller.ExplodeTile(x, y + 1, gameObject);
            caller.ExplodeTile(x, y - 1, gameObject);

            IsProcessing = true;
        }
        else if (isExploding)
        {
            if (blockedAbove && blockedBelow)
                IsDestroyed = true; //The bomb is subsequently destroyed
            else
            {
                distance++;

                //Adding sound of bomb exploding
                // Get audiolist
                m_audioList = GetComponent<AudioList>();
                if (m_audioList != null)
                    m_audioList.Play();

                if (!blockedAbove)
                    caller.ExplodeTile(x, y + distance, gameObject);
                if (!blockedBelow)
                    caller.ExplodeTile(x, y - distance, gameObject);

                //Check the surrounding tiles to see if any tile is blocking an explosion
                if (!blockedAbove && caller.IsBlockedTile(x, y + distance, this.gameObject))
                    blockedAbove = true;
                if (!blockedBelow && caller.IsBlockedTile(x, y - distance, this.gameObject))
                    blockedBelow = true;
            }

        }
        base.Tick(caller);
    }

    override public void OnExplosion(GameState caller)
    {
        base.OnExplosion(caller);
        if (explodeTick == -1)
        {
            explodeTick = caller.GetTickNumber() + 1;
        }
    }
}
