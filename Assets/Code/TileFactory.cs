﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileFactory : MonoBehaviour
{
    public static TileFactory Instance = null;
    private const char c_cellDelimiter = ',';

    [SerializeField]
    private AirTile m_airTile = null;

    [SerializeField]
    private CriticalBlockTile m_criticalBlockTile = null;

    [SerializeField]
    private DestructableBlockTile m_destructableBlockTile = null;

    [SerializeField]
    private ExplosiveBlockTile m_explosiveBlockTile = null;

    [SerializeField]
    private IndestructableBlockTile m_indestructableBlockTile = null;

    [SerializeField]
    private MoveableBlockTile m_moveableBlockTile = null;

    [SerializeField]
    private PlaceableBombTile m_placeableBombTile = null;

    [SerializeField]
    private DiamondBlockTile m_diamondBlockTile = null;

    [SerializeField]
    private HorizontalExplosiveBlockTile m_horizontalExplosiveBlockTile = null;

    [SerializeField]
    private VerticalExplosiveBlockTile m_verticalExplosiveBlockTile = null;

    [SerializeField]
    private FailTile m_failTile = null;

    [SerializeField]
    private FailTile m_altFailTile = null;
 
    [SerializeField]
    private InvalidTile m_invalidTile = null;
 
    [SerializeField]
    private string m_testString = null;

    private Tile CreateTileType<T>(T prefab, string tyleStr) where T : Tile
    {
        var tile = Instantiate<T>(prefab);
        if (tile.CanBeDamaged)
        {
            // health is stored as the second character to the type
            int healthAmount = 1;
            tile.AddHealthComponent(Int32.TryParse(tyleStr.Substring(1), out healthAmount) ? healthAmount : 1);
        }
        return tile;
    }

    private Tile CreateCriticalBlock(string tileType)
    {
        if (m_criticalBlockTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }

        var tile = CreateTileType<CriticalBlockTile>(m_criticalBlockTile, tileType);
        return tile;
    }

    private Tile CreateDestructableBlockTile(string tileType)
    {
        if (m_destructableBlockTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }

        var tile = CreateTileType<DestructableBlockTile>(m_destructableBlockTile, tileType);
        return tile;
    }

    private Tile CreatePlaceableBombTile(string tileType)
    {
        if (m_placeableBombTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }

        var tile = CreateTileType<PlaceableBombTile>(m_placeableBombTile, tileType);
        return tile;
    }

    private Tile CreateExplosiveBlockTile(string tileType)
    {
        if (m_explosiveBlockTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<ExplosiveBlockTile>(m_explosiveBlockTile, tileType);
        return tile;
    }

    private Tile CreateAirTile(string tileType)
    {
        if (m_airTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<AirTile>(m_airTile, tileType);
        return tile;
    }

    private Tile CreateIndestructableTile(string tileType)
    {
        if (m_indestructableBlockTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<IndestructableBlockTile>(m_indestructableBlockTile, tileType);
        return tile;
    }

    private Tile CreateMovableBlockTile(string tileType)
    {
        if (m_moveableBlockTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<MoveableBlockTile>(m_moveableBlockTile, tileType);
        return tile;
    }

    private Tile CreateDiamondBlockTile(string tileType)
    {
        if (m_diamondBlockTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<DiamondBlockTile>(m_diamondBlockTile, tileType);
        return tile;
    }

    private Tile CreateHorizontalExplosiveBlockTile(string tileType)
    {
        if (m_horizontalExplosiveBlockTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<HorizontalExplosiveBlockTile>(m_horizontalExplosiveBlockTile, tileType);
        return tile;
    }

    private Tile CreateVerticalExplosiveBlockTile(string tileType)
    {
        if (m_verticalExplosiveBlockTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<VerticalExplosiveBlockTile>(m_verticalExplosiveBlockTile, tileType);
        return tile;
    }

    private Tile CreateFailTile(string tileType, bool alt)
    {
        FailTile tilePrefab = (alt) ? m_altFailTile : m_failTile;
        if (tilePrefab == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<FailTile>(tilePrefab, tileType);
        return tile;
    }

    private Tile CreateInvalidTile(string tileType)
    {
        if (m_invalidTile == null)
        {
            Debug.Log("Missing prefab for tile");
            return null;
        }
        var tile = CreateTileType<InvalidTile>(m_invalidTile, tileType);
        return tile;
    }

    public Tile CreateTile(string tileType, int x, int y)
    {
        Tile tile = null;
        switch (tileType[0])
        {
            case 'C':
                tile = CreateCriticalBlock(tileType);
                break;
            case 'D':
                tile = CreateDestructableBlockTile(tileType);
                break;
            case 'P':
                tile = CreatePlaceableBombTile(tileType);
                break;
            case 'E':
                tile = CreateExplosiveBlockTile(tileType);
                break;
            case '_':
                tile = CreateAirTile(tileType);
                break;
            case '#':
                tile = CreateIndestructableTile(tileType);
                break;
            case 'M':
                tile = CreateMovableBlockTile(tileType);
                break;
            case '$':
                tile = CreateDiamondBlockTile(tileType);
                break;
            case 'V':
                tile = CreateHorizontalExplosiveBlockTile(tileType);
                break;
            case 'H':
                tile = CreateVerticalExplosiveBlockTile(tileType);
                break;
            case 'F':
                tile = CreateFailTile(tileType, (x + y) % 2 != 0);
                break;
        }

        if (tile == null)
        {
            GameObject gameObject = new GameObject();
            tile = gameObject.AddComponent<InvalidTile>();
        }

        tile.x = x;
        tile.y = y;

        return tile;
    }

    public Tile CreateReplacementOpenTile(int x, int y)
    {
        var tile = CreateAirTile("__");
        return tile;
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        if (string.IsNullOrEmpty(m_testString))
            return;

        // Test string
        string[] tileData = m_testString.Split(c_cellDelimiter);
        foreach (var tileStr in tileData)
            CreateTile(tileStr, 1, 1);
    }
}


