﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombUIHandler : MonoBehaviour {

    public GameObject m_canvas = null;

    public BombDropper m_surroundBombDropper = null;
    public BombDropper m_crossBombDropper = null;
    public BombDropper m_xBombDropper = null;
    public BombDropper m_pushBombDropper = null;

    public GameObject m_multiplierDisplay = null;
    public GameObject m_scoreDisplay = null;

    public float m_animTime = 0.6f;
    public float m_horizontalBase = 60.0f;
    public float m_horizontalSeparation = 80.0f;
    public float m_dropDownPos = 40.0f;

    public float m_multiplierWidth = 100.0f;
    public float m_scoreWidth = 120.0f;
    public float m_multiplierPosition = 60.0f;
    public float m_scorePosition = 140.0f;
    public float m_scoreAnimateTime = 0.2f;

    protected struct SDropper
    {
        public BombDropper instance;
        public int bombsLeft;
        public float fracVisible;

        public void Create(BombDropper source, Transform transform)
        {
            instance = Instantiate<BombDropper>(source, transform);
            bombsLeft = 0;
            fracVisible = 0.0f;
        }
    };

    protected struct SScoreBar
    {
        public GameObject instance;
        public UnityEngine.UI.Text text;
        public float visibleDuration;
        public float visibleTime;
        public float width;
        public float position;

        public void Create(GameObject source, Transform transform, float inWidth, float inPosition)
        {
            instance = Instantiate<GameObject>(source, transform);

            Transform tf = instance.transform.GetChild(0);
            if (tf != null && tf.gameObject != null)
                text = tf.gameObject.GetComponent(typeof(UnityEngine.UI.Text)) as UnityEngine.UI.Text;
            else
                text = null;

            visibleDuration = 0.0f;
            visibleTime = 0.0f;
            width = inWidth;
            position = inPosition;
        }

        public void Update(Canvas canvas, float animTime)
        {
            float fracVisible = 0.0f;

            if (visibleTime > 0.0f && UIManager.Instance.m_hudPanel.activeSelf)
            {
                visibleTime -= Time.deltaTime;
                if (visibleTime < animTime)
                    fracVisible = visibleTime / animTime;
                else if (visibleTime > visibleDuration - animTime)
                    fracVisible = (visibleDuration - visibleTime) / animTime;
                else
                    fracVisible = 1.0f;

                // Square to ease into and out of the visible position
                fracVisible = 1.0f - fracVisible;
                fracVisible = 1.0f - fracVisible * fracVisible;
            }

            float right = canvas.pixelRect.xMax - width * fracVisible;
            float top = canvas.pixelRect.yMax - position;

            RectTransform tf = instance.GetComponent(typeof(RectTransform)) as RectTransform;
            if (tf != null)
                tf.position = new Vector2(right, top);
        }
    };

    protected SDropper[] m_droppers;
    protected SScoreBar m_scoreBar;
    protected SScoreBar m_multiplierBar;

	// Use this for initialization
	void Start () {
        var h_scalar = Screen.width / 1024f;
        var v_scalar = Screen.height / 768f;

        m_droppers = new SDropper[4];
        m_scoreBar = new SScoreBar();
        m_multiplierBar = new SScoreBar();

        if (m_surroundBombDropper != null)
            m_droppers[0].Create(m_surroundBombDropper, m_canvas.transform);
        if (m_crossBombDropper != null)
            m_droppers[1].Create(m_crossBombDropper, m_canvas.transform);
        if (m_xBombDropper != null)
            m_droppers[2].Create(m_xBombDropper, m_canvas.transform);
        if (m_pushBombDropper != null)
            m_droppers[3].Create(m_pushBombDropper, m_canvas.transform);

        if (m_multiplierDisplay != null)
            m_multiplierBar.Create(m_multiplierDisplay, m_canvas.transform, m_multiplierWidth, m_multiplierPosition);
        if (m_scoreDisplay != null)
            m_scoreBar.Create(m_scoreDisplay, m_canvas.transform, m_scoreWidth, m_scorePosition);

        float startPos = m_horizontalBase;
        for (int i = 0; i < m_droppers.Length; i++)
        {
            RectTransform tf = m_droppers[i].instance.gameObject.GetComponent(typeof(RectTransform)) as RectTransform;
            if (tf != null)
                tf.position = new Vector2(startPos * h_scalar + i * m_horizontalSeparation * h_scalar, tf.position.y);
        }
	}

    // Update is called once per frame
    void Update()
    {
        var h_scalar = Screen.width / 1024f;
        var v_scalar = Screen.height / 768f;

         Canvas canvas = m_canvas.GetComponent(typeof(Canvas)) as Canvas;
        if (canvas == null)
            return;

        m_scoreBar.Update(canvas, m_scoreAnimateTime);
        m_multiplierBar.Update(canvas, m_scoreAnimateTime);

        float vPos = canvas.pixelRect.yMax - m_dropDownPos * v_scalar;

        float delta = Time.deltaTime / m_animTime;
        for (int i = 0; i < m_droppers.Length; i++)
        {
            if (m_droppers[i].bombsLeft > 0 && UIManager.Instance.m_hudPanel.activeSelf)
                m_droppers[i].fracVisible = Mathf.Min(m_droppers[i].fracVisible + delta, 1.0f);
            else
                m_droppers[i].fracVisible = Mathf.Max(m_droppers[i].fracVisible - delta, 0.0f);

            RectTransform tf = m_droppers[i].instance.gameObject.GetComponent(typeof(RectTransform)) as RectTransform;
            if (tf != null)
            {
                float targetHeight = Mathf.SmoothStep(vPos + tf.rect.height, vPos, m_droppers[i].fracVisible);
                tf.position = new Vector2(tf.position.x, targetHeight);
            }
        }
    }

    // Setup target values based on existing bombs
    public void SetupTargets(GameState gameState)
    {
        for (int i = 0; i < m_droppers.Length; i++)
            m_droppers[i].bombsLeft = 0;

        foreach (Bomb bomb in gameState.m_bombs)
        {
            if (bomb.posX >= 0)
                continue;

            for (int i = 0; i < m_droppers.Length; i++)
            {
                if (m_droppers[i].instance.m_type == bomb.bombType)
                {
                    m_droppers[i].bombsLeft ++;
                    break;
                }
            }
        }

        for (int i = 0; i < m_droppers.Length; i++)
        {
            Transform tf = m_droppers[i].instance.transform.GetChild(0);
            if (tf == null || tf.gameObject == null)
                continue;

            UnityEngine.UI.Text label = tf.gameObject.GetComponent(typeof(UnityEngine.UI.Text)) as UnityEngine.UI.Text;
            if (label == null)
                continue;

            label.text = m_droppers[i].bombsLeft.ToString();
        }
    }

    public void TriggerScoreDisplay(int multiplier, int score, float time)
    {
        if (score > 0)
        {
            if (multiplier > 1)
            {
                m_multiplierBar.visibleDuration = time;
                m_multiplierBar.visibleTime = time;
                m_multiplierBar.text.text = multiplier.ToString() + " X";
            }
            m_scoreBar.visibleDuration = time;
            m_scoreBar.visibleTime = time;
            m_scoreBar.text.text = score.ToString();
        }
    }

}
