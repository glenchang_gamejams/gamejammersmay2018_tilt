﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualSceneController : MonoBehaviour
{

    public float m_tileSize = 1.0f;
    public Vector3 m_boardCenter = new Vector3(0.0f, 0.0f, 0.0f);
    public float m_selectionShakeMagnitude = 0.1f;
    public float m_selectionShakeSpeed = 30.0f;
    public float m_turnTime = 1.0f;
    public float m_camShakeMagnitude = 0.4f;
    public float m_camShakeTimePerExplosion = 0.03f;
    public float m_cameraRotationTime = 0.5f;
    public int m_turnTimeDecrease = 8;
    public GameObject m_explosionType = null;
    public GameObject m_fuseSparkType = null;

    protected float m_cameraDistance;
    protected Vector3 m_cameraDirection;
    protected float m_camShakeTime;
    protected float m_averageTileScale;

    protected Bomb m_draggingBomb;
    protected GameObject m_bombFuseSpark;
    protected Bomb[] m_bombGrid;
    protected bool m_isIterating;
    protected bool m_firstTick;
    protected float m_currentTurnTime;
    protected int m_currentTurnIndex;

    protected float m_targetAngle;
    protected float m_cameraRotationTarget;
    protected float m_cameraRotationSource;
    protected float m_cameraRotationFrac;

    // Use this for initialization
    void Start()
    {
        m_cameraDistance = 10.0f;
        m_cameraDirection = new Vector3(0, 20, -8);
        m_cameraDirection.Normalize();
        m_draggingBomb = null;
        m_isIterating = false;
        m_camShakeTime = 0.0f;

        m_cameraRotationTarget = 0.0f;
        m_cameraRotationSource = 0.0f;
        m_cameraRotationFrac = 0.0f;

        EventManager.AddListener<LevelLoadedEvent>(OnLevelLoaded);
        EventManager.AddListener<UIStartTurnEvent>(OnUIStartTurn);
        EventManager.AddListener<UIRotateCameraLeftEvent>(OnUIRotateCameraLeft);
        EventManager.AddListener<UIRotateCameraRightEvent>(OnUIRotateCameraRight);
    }

    private void OnDestroy()
    {
        EventManager.RemoveListener<LevelLoadedEvent>(OnLevelLoaded);
        EventManager.RemoveListener<UIStartTurnEvent>(OnUIStartTurn);
        EventManager.RemoveListener<UIRotateCameraLeftEvent>(OnUIRotateCameraLeft);
        EventManager.RemoveListener<UIRotateCameraRightEvent>(OnUIRotateCameraRight);
    }

    float GetCurrentCameraRotation()
    {
        float frac = (1.0f - m_cameraRotationFrac);
        frac = 1.0f - frac * frac;

        return Mathf.Lerp(m_cameraRotationSource, m_cameraRotationTarget, frac);
    }

    // Update is called once per frame
    void Update()
    {
        GameState gameState = this.gameObject.GetComponent(typeof(GameState)) as GameState;
        BombUIHandler uiHandler = this.gameObject.GetComponent(typeof(BombUIHandler)) as BombUIHandler;
        if (gameState == null || uiHandler == null)
            return;

        UpdateCamera();

        UpdateBombGrid(gameState, uiHandler);
        UpdateTileMeshPositions(gameState);

        int tileHover = GetTileFromScreenPoint(Input.mousePosition, gameState, m_draggingBomb != null);

        UpdateBombs(gameState, uiHandler, Input.mousePosition, tileHover);

        float Shake = m_tileSize * m_selectionShakeMagnitude;
        if (tileHover >= 0)
        {
            Vector3 offset = new Vector3(0, Mathf.Cos(Time.time * m_selectionShakeSpeed) * Shake, 0);
            gameState.m_tileList[tileHover].gameObject.transform.position += offset;
            if (m_bombGrid[tileHover] != null)
                m_bombGrid[tileHover].gameObject.transform.position += offset;
            else if (m_draggingBomb != null && gameState.m_tileList[tileHover].CanHoldBomb)
                m_draggingBomb.gameObject.transform.position += offset;
        }

        if (m_isIterating)
            UpdateIteration(gameState, uiHandler);
    }

    public void UpdateCamera()
    {
        Vector3 camDir = m_cameraDirection * m_cameraDistance;
        if (m_cameraRotationTarget != m_cameraRotationSource)
        {
            m_cameraRotationFrac += Time.deltaTime / m_cameraRotationTime;
            if (m_cameraRotationFrac > 1.0f)
            {
                m_cameraRotationSource = m_cameraRotationTarget;
                m_cameraRotationFrac = 0.0f;
            }
        }
        camDir = Quaternion.AngleAxis(GetCurrentCameraRotation(), new Vector3(0.0f, 1.0f, 0.0f)) * camDir;
        Camera.main.transform.position = m_boardCenter + camDir;

        if (m_camShakeTime > 0.0f)
        {
            float range = m_camShakeTime * m_camShakeMagnitude;
            Vector3 offset = new Vector3(Random.Range(-range, range), Random.Range(-range, range), Random.Range(-range, range));
            Camera.main.transform.position += offset;
            Camera.main.transform.LookAt(m_boardCenter + offset);
            m_camShakeTime -= Time.deltaTime;
        }
        else
            Camera.main.transform.LookAt(m_boardCenter);
    }

    public void OnLevelLoaded(LevelLoadedEvent evt)
    {
        SetCameraRotationTarget(45.0f);

        GameState gameState = this.gameObject.GetComponent(typeof(GameState)) as GameState;
        BombUIHandler uiHandler = this.gameObject.GetComponent(typeof(BombUIHandler)) as BombUIHandler;
        if (gameState == null || uiHandler == null)
            return;

        uiHandler.SetupTargets(gameState);
    }

    public void ResetBombs()
    {
        GameState gameState = this.gameObject.GetComponent(typeof(GameState)) as GameState;
        BombUIHandler uiHandler = this.gameObject.GetComponent(typeof(BombUIHandler)) as BombUIHandler;
        if (gameState == null || uiHandler == null)
            return;

        foreach (Bomb bomb in gameState.m_bombs)
        {
            bomb.posX = GameState.c_invalidBombPos;
            bomb.posY = GameState.c_invalidBombPos;

            // Reset fuse to remove overlay
            bomb.fuse = 0;

            if (m_fuseSparkType == null)
                continue;

            int childCount = bomb.transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                Transform tf = bomb.transform.GetChild(i);
                if (tf == null || tf.gameObject == null)
                    continue;

                if (tf.gameObject.name.Contains(m_fuseSparkType.name))
                {
                    GameObject.Destroy(tf.gameObject);
                }
            }
        }
        uiHandler.SetupTargets(gameState);
    }

    // Convert a screen position to a tile
    int GetTileFromScreenPoint(Vector2 pos, GameState gameState, bool ignoreNonPlacementTiles)
    {
        Ray ray = Camera.main.ScreenPointToRay(pos);

        int result = -1;
        float hitDist = float.MaxValue;

        for (int i = 0; i < gameState.m_tileList.Length; i++)
        {
            Tile tile = gameState.m_tileList[i];
            if (ignoreNonPlacementTiles && !tile.CanHoldBomb)
                continue;

            MeshFilter meshFilter = tile.gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
            if (meshFilter == null || meshFilter.mesh == null)
                continue;

            Bounds bounds = meshFilter.mesh.bounds;
            bounds.extents *= tile.gameObject.transform.localScale.x;
            bounds.center += tile.gameObject.transform.position;
            float testDist = float.MaxValue;
            bool hit = bounds.IntersectRay(ray, out testDist);

            // This is a bit unwieldy, but makes sure that we're prioritizing
            //  tiles with bombs over tiles without them
            if (!hit)
                continue;

            if (result > 0)
            {
                if (m_bombGrid[i] == null && m_bombGrid[result] != null)
                    continue;

                if (m_bombGrid[i] == m_bombGrid[result] && testDist > hitDist)
                    continue;
            }

            hitDist = testDist;
            result = i;
        }
        return result;
    }

    // Start iteration, keep going until there's no active bombs
    public void StartIterating()
    {
        if (m_isIterating)
            return;

        m_isIterating = true;
        m_currentTurnTime = m_turnTime;
        m_firstTick = true;
        m_currentTurnIndex = 0;
    }

    // generate locally stored bomb grid
    void UpdateBombGrid(GameState gameState, BombUIHandler uiHandler)
    {
        if (m_bombGrid == null || m_bombGrid.Length != gameState.m_tileList.Length)
        {
            m_bombGrid = new Bomb[gameState.m_tileList.Length];
            uiHandler.SetupTargets(gameState);
        }

        for (int i = 0; i < m_bombGrid.Length; i++)
            m_bombGrid[i] = null;

        foreach (Bomb bomb in gameState.m_bombs)
        {
            if (bomb.posX >= 0)
                m_bombGrid[bomb.posX + bomb.posY * gameState.m_boardWidth] = bomb;
        }
    }

    protected Vector3 TilePosToWorld(int x, int y, GameState gameState)
    {
        return new Vector3(x * m_tileSize - gameState.m_boardWidth * m_tileSize / 2.0f,
                0.0f,
                y * m_tileSize - gameState.m_boardHeight * m_tileSize / 2.0f);
    }

    // Updates mesh positions of tiles
    void UpdateTileMeshPositions(GameState gameState)
    {
        float scaleCount = 0.0f;
        m_averageTileScale = 0.0f;
        for (int i = 0; i < gameState.m_tileList.Length; i++)
        {
            Tile tile = gameState.m_tileList[i];

            MeshFilter meshFilter = tile.gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
            int tileX = i % gameState.m_boardWidth;
            int tileY = i / gameState.m_boardWidth;
            float scale = 1.0f;
            float tileHeight = 0.0f;
            if (meshFilter != null && meshFilter.mesh != null)
            {
                Bounds bounds = meshFilter.mesh.bounds;

                scale = Mathf.Min(m_tileSize / (bounds.extents.x * 2.0f), m_tileSize / (bounds.extents.z * 2.0f));
                m_averageTileScale += scale;
                scaleCount += 1.0f;
                tileHeight = bounds.extents.y * scale;
            }

            tile.gameObject.transform.localScale = new Vector3(scale, scale, scale);
            tile.gameObject.transform.position = TilePosToWorld(tileX, tileY, gameState) + new Vector3(0.0f, tileHeight, 0.0f);
        }
        m_averageTileScale /= scaleCount;

    }

    void UpdateBombs(GameState gameState, BombUIHandler uiHandler, Vector2 pos, int tileHover)
    {
        int validTileHover = (tileHover >= 0 && gameState.m_tileList[tileHover].CanHoldBomb && m_bombGrid[tileHover] == null) ? tileHover : -1;

        float draggingHeight = 0.0f;
        foreach (Bomb bomb in gameState.m_bombs)
        {
            MeshRenderer render = bomb.gameObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
            MeshFilter meshFilter = bomb.gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
            if (render == null || meshFilter == null || meshFilter.mesh == null)
                continue;

            Bounds bounds = meshFilter.mesh.bounds;
            float scale = Mathf.Min(m_tileSize / (bounds.extents.x * 2.0f), Mathf.Min(m_tileSize / (bounds.extents.y * 2.0f), m_tileSize / (bounds.extents.z * 2.0f)));
            bomb.gameObject.transform.localScale = new Vector3(scale, scale, scale);

            if (bomb == m_draggingBomb)
            {
                bomb.gameObject.SetTransparent((validTileHover >= 0) ? 0.6f : 0.3f);
                draggingHeight = bounds.extents.y * scale;
                render.enabled = true;
            }
            else if (bomb.posX < 0)
            {
                render.enabled = false;
            }
            else
            {
                bomb.gameObject.SetOpaque();
                bomb.gameObject.transform.position = gameState.m_tileList[bomb.posX + bomb.posY * gameState.m_boardWidth].transform.position + new Vector3(0, bounds.extents.y * scale, 0);
                render.enabled = true;
            }
        }

        if (m_draggingBomb != null)
        {
            if (validTileHover >= 0)
            {
                m_draggingBomb.gameObject.transform.position = gameState.m_tileList[validTileHover].transform.position + new Vector3(0, draggingHeight, 0);
            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(pos);
                Vector3 intPoint = ray.origin - ray.direction * ((ray.origin.y + m_boardCenter.y - m_tileSize * 1.5f) / ray.direction.y);
                m_draggingBomb.gameObject.transform.position = intPoint;
            }

            if (!Input.GetMouseButton(0))
            {
                PlayBombInteractAudio();
                if (validTileHover >= 0)
                {
                    if (m_draggingBomb.posX < 0)
                        m_draggingBomb.fuse = gameState.CountActiveBombs() + 1;
                    m_draggingBomb.posX = validTileHover % gameState.m_boardWidth;
                    m_draggingBomb.posY = validTileHover / gameState.m_boardWidth;
                }

                if (m_bombFuseSpark != null)
                {
                    if (m_draggingBomb.posX < 0)
                        GameObject.Destroy(m_bombFuseSpark);
                    m_bombFuseSpark = null;
                }

                m_draggingBomb = null;
                uiHandler.SetupTargets(gameState);

            }
        }
        else if (Input.GetMouseButtonDown(0) && tileHover > 0 && m_bombGrid[tileHover] != null)
        {
            StartBombDrag(m_bombGrid[tileHover]);
        }
    }

    public void UpdateIteration(GameState gameState, BombUIHandler uiHandler)
    {
        m_currentTurnTime += Time.deltaTime;
        if (m_currentTurnTime >= m_turnTime / (1.0f + (m_currentTurnIndex / m_turnTimeDecrease)))
        {
            if (gameState.IsProcessing())
            {
                gameState.Tick();
                m_currentTurnIndex ++;
                m_currentTurnTime = 0.0f;
                m_camShakeTime += gameState.m_explodedTilesLastFrame.Count * m_camShakeTimePerExplosion;

                if (m_firstTick)
                {
                    gameState.m_scoreTracker.m_ignitionCount++;
                    m_firstTick = false;
                }

                if (m_explosionType != null)
                {
                    foreach (int tileIndex in gameState.m_explodedTilesLastFrame)
                    {
                        GameObject newObject = Instantiate<GameObject>(m_explosionType);
                        newObject.transform.position = TilePosToWorld(tileIndex % gameState.m_boardWidth, tileIndex / gameState.m_boardWidth, gameState) + new Vector3(0, m_tileSize * 0.5f, 0);
                        newObject.transform.localScale = new Vector3(m_averageTileScale, m_averageTileScale, m_averageTileScale);
                    }
                }

                uiHandler.TriggerScoreDisplay(gameState.m_scoreTracker.m_lastFrameMultiplier, gameState.m_scoreTracker.m_lastFrameScore, m_turnTime - 0.1f);
            }
            else
            {
                m_isIterating = false;
                EventManager.Fire(new TurnEndEvent(gameState));
            }
        }
    }

    public void PlayBombInteractAudio()
    {
        GameObject audioObj = GameObject.FindGameObjectWithTag("BombAudio");
        if (audioObj != null)
        {
            var audioList = audioObj.GetComponent<AudioList>();
            if (audioList != null)
                audioList.Play();
        }
    }

    public void StartBombDrag(Bomb bomb)
    {
        PlayBombInteractAudio();
        m_draggingBomb = bomb;
        if (m_fuseSparkType != null)
        {
            m_bombFuseSpark = Instantiate<GameObject>(m_fuseSparkType, m_draggingBomb.gameObject.transform);
        }
    }

    public void SetCameraRotationTarget(float target)
    {
        m_cameraRotationTarget = target;

        // Make sure we're in the -180 .. 180 range
        while (m_cameraRotationTarget < -180.0f)
            m_cameraRotationTarget += 360.0f;
        while (m_cameraRotationTarget > 180.0f)
            m_cameraRotationTarget -= 360.0f;

        // Make sure we're always taking the shortest path between angles
        //  this means m_cameraRotationSource will sometimes be outside the -180 .. 180 range,
        //  but it will be in the -360 .. 360 range
        while (m_cameraRotationSource > m_cameraRotationTarget + 180.0f)
            m_cameraRotationSource -= 360.0f;
        while (m_cameraRotationSource < m_cameraRotationTarget - 180.0f)
            m_cameraRotationSource += 360.0f;
    }

    public void Rotate(int direction)
    {
        m_cameraRotationSource = GetCurrentCameraRotation();
        m_cameraRotationFrac = 0.0f;
        SetCameraRotationTarget(m_cameraRotationTarget + direction * 45.0f);
    }

    public void OnUIStartTurn(UIStartTurnEvent evt)
    {
        StartIterating();
    }

    public void OnUIRotateCameraLeft(UIRotateCameraLeftEvent evt)
    {
        Rotate(1);
    }

    public void OnUIRotateCameraRight(UIRotateCameraRightEvent evt)
    {
        Rotate(-1);
    }

}
