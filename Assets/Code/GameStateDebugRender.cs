﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateDebugRender : MonoBehaviour {

    protected LineRenderer m_outline;
    protected LineRenderer m_bombHighlight;

    protected LineRenderer[] m_tileSquares;

    protected LineRenderer[] m_bombs;

    protected LineRenderer[] m_explosions;

    public Vector2 m_size = new Vector2(3.0f, 3.0f);
    public Vector2 m_center = new Vector2(0.0f, 0.0f);

    public float m_lineWidthThick = 0.1f;
    public float m_lineWidthThin = 0.05f;

    private bool m_mouseDown;
    private int m_bombHighlightIndex;

    void SetupLines(LineRenderer line, int count, float width, bool loop)
    {
        line.sortingLayerName = "OnTop"; 
        line.material = new Material(Shader.Find("Sprites/Default"));
        line.sortingOrder = 5;
        line.positionCount = count;
        line.startWidth = line.endWidth = width;
        line.useWorldSpace = true;
        line.loop = loop;
    }

    void CreateSquare(LineRenderer line, float width, Vector2 min, Vector2 max)
    {
        SetupLines(line, 4, width, true);

        line.SetPosition(0, new Vector3(min.x, 0.0f, min.y));
        line.SetPosition(1, new Vector3(max.x, 0.0f, min.y));
        line.SetPosition(2, new Vector3(max.x, 0.0f, max.y));
        line.SetPosition(3, new Vector3(min.x, 0.0f, max.y));
    }

    void CreateBomb(LineRenderer line, float width, Vector2 pos, float radius, float fuseLength)
    {
        SetupLines(line, 10, width, false);

        float invSq2 = 0.71f * radius;
        line.SetPosition(0, new Vector3(pos.x, 0.01f, pos.y + radius));
        line.SetPosition(1, new Vector3(pos.x - invSq2, 0.01f, pos.y + invSq2));
        line.SetPosition(2, new Vector3(pos.x - radius, 0.01f, pos.y));
        line.SetPosition(3, new Vector3(pos.x - invSq2, 0.01f, pos.y - invSq2));
        line.SetPosition(4, new Vector3(pos.x, 0.01f, pos.y - radius));
        line.SetPosition(5, new Vector3(pos.x + invSq2, 0.01f, pos.y - invSq2));
        line.SetPosition(6, new Vector3(pos.x + radius, 0.01f, pos.y));
        line.SetPosition(7, new Vector3(pos.x + invSq2, 0.01f, pos.y + invSq2));
        line.SetPosition(8, new Vector3(pos.x, 0.01f, pos.y + radius));
        line.SetPosition(9, new Vector3(pos.x, 0.01f, pos.y + radius + fuseLength));
    }

    void CreateExplosion(LineRenderer line, float width, Vector2 pos, float radius)
    {
        SetupLines(line, 16, width, true);

        float invSq2 = 0.71f * radius;
        float offs0 = radius * 0.2f;
        float offs1 = offs0 * 2.0f;
        line.SetPosition(0, new Vector3(pos.x, 0.02f, pos.y + radius));
        line.SetPosition(1, new Vector3(pos.x - offs0, 0.02f, pos.y + offs1));
        line.SetPosition(2, new Vector3(pos.x - invSq2, 0.02f, pos.y + invSq2));
        line.SetPosition(3, new Vector3(pos.x - offs1, 0.02f, pos.y + offs0));
        line.SetPosition(4, new Vector3(pos.x - radius, 0.02f, pos.y));
        line.SetPosition(5, new Vector3(pos.x - offs1, 0.02f, pos.y - offs0));
        line.SetPosition(6, new Vector3(pos.x - invSq2, 0.02f, pos.y - invSq2));
        line.SetPosition(7, new Vector3(pos.x - offs0, 0.02f, pos.y - offs1));
        line.SetPosition(8, new Vector3(pos.x, 0.02f, pos.y - radius));
        line.SetPosition(9, new Vector3(pos.x + offs0, 0.02f, pos.y - offs1));
        line.SetPosition(10, new Vector3(pos.x + invSq2, 0.02f, pos.y - invSq2));
        line.SetPosition(11, new Vector3(pos.x + offs1, 0.02f, pos.y - offs0));
        line.SetPosition(12, new Vector3(pos.x + radius, 0.02f, pos.y));
        line.SetPosition(13, new Vector3(pos.x + offs1, 0.02f, pos.y + offs0));
        line.SetPosition(14, new Vector3(pos.x + invSq2, 0.02f, pos.y + invSq2));
        line.SetPosition(15, new Vector3(pos.x + offs0, 0.02f, pos.y + offs1));
    }

	// Use this for initialization
	void Start () {
        m_outline = this.gameObject.AddComponent<LineRenderer>();
        Vector2 padding = new Vector2(m_lineWidthThick, m_lineWidthThick);
        CreateSquare(m_outline, m_lineWidthThick, m_center - m_size * 0.5f - padding, m_center + m_size * 0.5f + padding);

        Regenerate();
        m_bombHighlightIndex = -1;
	}

    void ReleaseLineList(LineRenderer[] list)
    {
        if (list == null)
            return;

        for (int i = 0; i < list.Length; i++)
            GameObject.Destroy(list[i].gameObject);
        list = null;
    }

    protected Vector2 TilePosToWorld(int x, int y, GameState gameState)
    {
        return m_center - m_size * 0.5f + 
            new Vector2(
                (m_size.x * (x + 0.5f)) / gameState.m_boardWidth, 
                (m_size.y * (gameState.m_boardHeight - y - 1 + 0.5f)) / gameState.m_boardHeight
            );
    }

    protected int WorldToTileIndex(float x, float y, GameState gameState)
    {
        float xLoc = x - m_center.x + m_size.y * 0.5f;
        float yLoc = y - m_center.y + m_size.y * 0.5f;

        if (xLoc < 0.0f || yLoc < 0.0f || xLoc >= m_size.x || yLoc >= m_size.y)
            return -1;

        int xTile = (int)(xLoc * gameState.m_boardWidth / m_size.x);
        int yTile = (int)(gameState.m_boardHeight - (yLoc * gameState.m_boardHeight / m_size.y));

        return xTile + yTile * gameState.m_boardWidth;
    }

    protected Vector2 IdleBombPos(int bombIndex, float bombRadius)
    {
        return m_center - m_size * 0.5f + new Vector2(-bombRadius * 4.0f, (bombIndex + 0.2f) * bombRadius * 4.0f);
    }

    protected Vector2 TileDim(GameState gameState)
    {
        return new Vector2(m_size.x / gameState.m_boardWidth, m_size.y / gameState.m_boardHeight);
    }

    protected float BombRadius(GameState gameState)
    {
        return Vector2.Dot(TileDim(gameState), new Vector2(0.15f, 0.15f));
    }

    protected void ClearBombHighlight()
    {
        m_bombHighlightIndex = -1;
        if (m_bombHighlight != null)
        {
            GameObject.Destroy(m_bombHighlight.gameObject);
            m_bombHighlight = null;
        }
    }

    public void Regenerate()
    {
        GameState gameState = this.gameObject.GetComponent(typeof(GameState)) as GameState;
        if (gameState == null)
            return;

        ReleaseLineList(m_tileSquares);
        ReleaseLineList(m_bombs);
        ReleaseLineList(m_explosions);

        if (gameState.m_tileList != null)
        {
            m_tileSquares = new LineRenderer[gameState.m_tileList.Length];
            Vector2 tileHalfDim = TileDim(gameState) * 0.5f - new Vector2(m_lineWidthThin, m_lineWidthThin);
            for (int i = 0; i < m_tileSquares.Length; i++)
            {
                MeshRenderer render = gameState.m_tileList[i].gameObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
                if (render != null)
                    Destroy(render);
                Vector2 locPos = TilePosToWorld((i % gameState.m_boardWidth), (i / gameState.m_boardWidth), gameState);
                GameObject newGameObject = new GameObject("GFXDEBUG tile " + i);
                m_tileSquares[i] = newGameObject.AddComponent<LineRenderer>();
                CreateSquare(m_tileSquares[i], m_lineWidthThin, locPos - tileHalfDim, locPos + tileHalfDim);
            }

            for (int i = 0; i < m_tileSquares.Length; i++)
            {
                Color color = (gameState.m_tileList[i] != null) ? gameState.m_tileList[i].GetDebugColor() : Color.blue;
                m_tileSquares[i].startColor = color;
                m_tileSquares[i].endColor = color;
            }
        }
        else
            m_tileSquares = null;

        if (gameState.m_bombs != null && gameState.m_bombs.Count > 0)
        {
            int bombCount = 0;
            float bombRadius = BombRadius(gameState);
            float fuseMultiplier = bombRadius * 0.5f;
            m_bombs = new LineRenderer[gameState.m_bombs.Count];
            int idleBombCount = 0;
            foreach (Bomb bomb in gameState.m_bombs)
            {
                MeshRenderer render = bomb.gameObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
                if (render != null)
                    Destroy(render);

                GameObject newGameObject = new GameObject("GFXDEBUG bomb " + bomb.posX + ", " + bomb.posY);
                m_bombs[bombCount] = newGameObject.AddComponent<LineRenderer>();
                if (bomb.posX < 0)
                {
                    CreateBomb(m_bombs[bombCount], m_lineWidthThin, IdleBombPos(idleBombCount, bombRadius), bombRadius, bomb.fuse * fuseMultiplier);
                    idleBombCount++;
                }
                else
                    CreateBomb(m_bombs[bombCount], m_lineWidthThin, TilePosToWorld(bomb.posX, bomb.posY, gameState), bombRadius, bomb.fuse * fuseMultiplier);
                m_bombs[bombCount].startColor = m_bombs[bombCount].endColor = bomb.debugColor;
                bombCount++;
            }
        }
        else
            m_bombs = null;

        if (gameState.m_explodedTilesLastFrame != null && gameState.m_explodedTilesLastFrame.Count > 0)
        {
            int explosionCount = 0;
            float explosionRadius = Vector2.Dot(TileDim(gameState), new Vector2(0.22f, 0.22f));
            m_explosions = new LineRenderer[gameState.m_explodedTilesLastFrame.Count];
            foreach (int index in gameState.m_explodedTilesLastFrame)
            {
                GameObject newGameObject = new GameObject("GFXDEBUG explosion " + index);
                m_explosions[explosionCount] = newGameObject.AddComponent<LineRenderer>();
                CreateExplosion(m_explosions[explosionCount], m_lineWidthThin, TilePosToWorld(index % gameState.m_boardWidth, index / gameState.m_boardWidth, gameState), explosionRadius);
                m_explosions[explosionCount].startColor = m_explosions[explosionCount].endColor = Color.red;
                explosionCount++;
            }
        }
        else
            m_explosions = null;
    }

    // Update is called once per frame
    void Update()
    {
        GameState gameState = this.gameObject.GetComponent(typeof(GameState)) as GameState;
        if (gameState == null)
            return;

        if (m_tileSquares == null || m_tileSquares.Length == 0)
            Regenerate();

        if (Input.GetMouseButtonDown(0))
        {
            if (!m_mouseDown)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Vector3 intPoint = ray.origin + ray.direction * ray.origin.y;
                Vector2 mousePt = new Vector2(intPoint.x, intPoint.z);

                int tileIndex = WorldToTileIndex(mousePt.x, mousePt.y, gameState);
                int tileX = tileIndex % gameState.m_boardWidth;
                int tileY = tileIndex / gameState.m_boardWidth;

                if (tileIndex >= 0)
                {
                    foreach (Bomb bomb in gameState.m_bombs)
                    {
                        if (bomb.posX == tileX && bomb.posY == tileY)
                        {
                            bomb.posY = bomb.posX = GameState.c_invalidBombPos;

                            Regenerate();
                            ClearBombHighlight();
                        }
                    }

                    if (m_bombHighlightIndex >= 0 && gameState.m_tileList[tileIndex].CanHoldBomb)
                    {
                        Bomb bomb = gameState.m_bombs[m_bombHighlightIndex];
                        bomb.posX = tileX;
                        bomb.posY = tileY;

                        Regenerate();
                        ClearBombHighlight();
                    }
                }

                // Clicked bomb highlight
                float bombRadius = BombRadius(gameState);

                int idleBombCount = 0;
                int bombIndex = 0;
                foreach (Bomb bomb in gameState.m_bombs)
                {
                    if (bomb.posX >= 0)
                    {
                        bombIndex++;
                        continue;
                    }

                    Vector2 bombPos = IdleBombPos(idleBombCount, bombRadius);
                    float dist = (bombPos - mousePt).sqrMagnitude;
                    if (dist < bombRadius * bombRadius)
                    {
                        ClearBombHighlight();
                        m_bombHighlightIndex = bombIndex;
                        GameObject newGameObject = new GameObject("GFXDEBUG bomb highlight");
                        m_bombHighlight = newGameObject.AddComponent<LineRenderer>();
                        CreateSquare(m_bombHighlight, m_lineWidthThin, bombPos - new Vector2(bombRadius * 1.4f, bombRadius * 1.4f), bombPos + new Vector2(bombRadius * 1.4f, bombRadius * 1.4f));
                        m_bombHighlight.startColor = Color.white;
                        m_bombHighlight.endColor = Color.white;
                    }
                    idleBombCount++;
                    bombIndex++;
                }
            }
            m_mouseDown = true;
        }
        else
            m_mouseDown = false;
    }
}
