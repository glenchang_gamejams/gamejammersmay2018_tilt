﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    public const int c_invalidBombPos = -1000;

    public int Level = 0;

    public int m_boardWidth;
    public int m_boardHeight;

    public Tile[] m_tileList;

    public GameObject m_backgroundObject;

    public TileFactory m_tileFactory;
    public BombFactory m_bombFactory;


    public List<Bomb> m_bombs;

    protected int m_tickNumber;

    public struct SScoreTracker
    {
        public int m_largestExplosion;
        public int m_ignitionCount;
        public int m_blocksDestroyed;
        public int m_mostBlocksDestroyedInTick;
        public int m_longestChain;
        public int m_totalScore;

        public int m_lastFrameScore;
        public int m_lastFrameMultiplier;

        public void Reset()
        {
            m_largestExplosion = 0;
            m_ignitionCount = 0;
            m_blocksDestroyed = 0;
            m_mostBlocksDestroyedInTick = 0;
            m_longestChain = 0;
            m_totalScore = 0;
            m_lastFrameScore = 0;
            m_lastFrameMultiplier = 0;
        }

        public int GetMultiplier(int explosionSize, int blocksDestroyed, int chainLength)
        {
            return 1 + (explosionSize / 5);
        }

        public void AddFrameScore(int explosionSize, int blocksDestroyed, int chainLength)
        {
            m_largestExplosion = Math.Max(explosionSize, m_largestExplosion);

            m_mostBlocksDestroyedInTick = Math.Max(blocksDestroyed, m_mostBlocksDestroyedInTick);
            m_blocksDestroyed += blocksDestroyed;

            m_longestChain = Math.Max(chainLength, m_longestChain);

            m_lastFrameMultiplier = GetMultiplier(explosionSize, blocksDestroyed, chainLength);
            m_lastFrameScore = blocksDestroyed * m_lastFrameMultiplier;
            m_totalScore += m_lastFrameScore;
        }
    };
    public SScoreTracker m_scoreTracker;
    protected int m_currentDestroyedBlockCount;

    // Tracking
    public List<int> m_explodedTilesLastFrame;

    public void ClearBoard()
    {
        if (m_backgroundObject != null)
            Destroy(m_backgroundObject);

        for (int i = 0; i < m_tileList.Length; i++)
        {
            Destroy(m_tileList[i].gameObject);
        }

        for (int i = 0; i < m_bombs.Count; i++)
        {
            Destroy(m_bombs[i].gameObject);
        }

        Array.Clear(m_tileList, 0, m_tileList.Length);
        m_bombs.Clear();
    }

    public void LoadBoard(int level)
    {
        Level = level;
        // Clear the board first
        ClearBoard();

        BoardDatabase.BoardData board = BoardDatabase.Instance.GetBoard(level);
        if (board == null)
            return;

        m_boardWidth = board.boardWidth;
        m_boardHeight = board.boardHeight;
        m_tileList = new Tile[m_boardWidth * m_boardHeight];

        // TileFactory tileFactory = this.gameObject.GetComponent(typeof(TileFactory)) as TileFactory;
        TileFactory tileFactory = m_tileFactory;
        if (tileFactory != null)
        {
            string[][] tiles = board.tiles;
            for (int x = 0; x < tiles.Length; x++)
            {
                for (int y = 0; y < tiles[x].Length; y++)
                {
                    m_tileList[x + y * m_boardWidth] = tileFactory.CreateTile(tiles[x][y], x, y);
                }
            }
        }

        //if (m_bombFactory != null)
        {
            string[] bombs = board.bombTypes;
            m_bombs = new List<Bomb>();
            for (int i = 0; i < bombs.Length; i++)
                m_bombFactory.CreateBomb(bombs[i],m_bombs);
            for (int i = 0; i < m_bombs.Count; i++)
            {
                m_bombs[i].posX = c_invalidBombPos;
                m_bombs[i].posY = c_invalidBombPos;
            }
        }

        // Create background
        var backgrounds = BoardDatabase.Instance.m_backgrounds;
        if (backgrounds != null && backgrounds.Length > 0)
        {
            m_backgroundObject = Instantiate(backgrounds[Level % backgrounds.Length]);
        }

        m_scoreTracker.Reset();
        EventManager.Fire(new LevelLoadedEvent(Level));
    }

    public int GetTickNumber()
    {
        return m_tickNumber;
    }

    // Use this for initialization
    public void Start()
    {
        // TEMP
        m_tickNumber = 0;
        EventManager.AddListener<StartLevelEvent>(OnStartLevel);
        EventManager.AddListener<RestartGameEvent>(OnRestartGame);
        m_scoreTracker = new SScoreTracker();
    }

    public void OnDestroy()
    {
        EventManager.RemoveListener<StartLevelEvent>(OnStartLevel);
        EventManager.RemoveListener<RestartGameEvent>(OnRestartGame);
    }

    private void OnRestartGame(RestartGameEvent evt)
    {
        ClearBoard();
    }

    public void OnStartLevel(StartLevelEvent evt)
    {
        LoadBoard(evt.Level);
    }

    // Update is called once per frame
    public void Update() {

    }

    public void ExplodeTile(int x, int y, GameObject caller)
    {
        if (x >= 0 && y >= 0 && x < m_boardWidth && y < m_boardHeight)
        {
            int tileIndex = x + y * m_boardWidth;
            m_tileList[tileIndex].OnExplosion(this);
            m_explodedTilesLastFrame.Add(tileIndex);
        }
    }

    public bool IsBlockedTile(int x, int y, GameObject caller)
    {
        if (x < 0 || y < 0 || x >= m_boardWidth || y >= m_boardHeight)
            return true;

        int tileIndex = x + y * m_boardWidth;

        // Check if out of range
        if (tileIndex < 0 || tileIndex > m_tileList.Length)
            return true;

        if (tileIndex < m_tileList.Length && m_tileList[tileIndex].IsBlockingExplosion)
            return true;

        for (int i = 0; i < m_bombs.Count; i++)
        {
            if (m_bombs[i] != caller && m_bombs[i].posX == x && m_bombs[i].posY == y && m_bombs[i].IsBlockingBomb())
                return true;
        }
            
        return false;
    }

    public void ReplaceTile(int x, int y, Tile tile)
    {
        int tileIndex = x + y * m_boardWidth;
        if (tileIndex < m_tileList.Length)
        {
            GameObject.Destroy(m_tileList[tileIndex].gameObject);
            m_tileList[tileIndex] = tile;
            tile.x = x;
            tile.y = y;
        }
        else
            Debug.Log("Attempted to replace invalid tile at location " + x + ", " + y);
    }

    public void PushObject(int sourceX, int sourceY, int destX, int destY, GameObject caller)
    {
        //Check we are not pushing to boundary:
        if (destX < 0 || destY < 0 || destX >= m_boardWidth || destY >= m_boardHeight)
            return;

        //Check that the destination is not blocked:
        if (IsBlockedTile(destX, destY, caller))
            return;

        //Check that the source block is not empty
        if (!IsBlockedTile(sourceX, sourceY, caller))
            return;

        //If a bomb is supposed to move into the destination position, move it
        for (int i = 0; i < m_bombs.Count; i++)
        {
            if (m_bombs[i] != caller && m_bombs[i].posX == sourceX && m_bombs[i].posY == sourceY)
            {
                m_bombs[i].posX = destX;
                m_bombs[i].posY = destY;
                return;
            }
        }

        //Otherwise, if a block tile is supposed to move, move it
        int tileIndex = sourceX + sourceY * m_boardWidth;
        ReplaceTile(destX, destY, m_tileList[tileIndex]);

        //Create replacement tile
        var placeable = TileFactory.Instance.CreateReplacementOpenTile(sourceX, sourceY);
        m_tileList[tileIndex] = placeable;

    }
    public void Tick()
    {
        GameManager.Instance.Tick(this);

        // Clean up dead bombs
        for (int i = 0; i < m_bombs.Count; i++)
        {
            if (m_bombs[i].IsDestroyed())
            {
                GameObject.Destroy(m_bombs[i].gameObject);
                m_bombs.RemoveAt(i);
                i--;
            }
        }

        m_currentDestroyedBlockCount = 0;
        m_explodedTilesLastFrame.Clear();
        for (int i = 0; i < m_boardWidth * m_boardHeight; i++)
        {
            m_tileList[i].Tick(this);
        }

        foreach (Bomb bomb in m_bombs)
        {
            if (bomb.posX != c_invalidBombPos)
                bomb.Tick(this);
        }

        // Defer bomb explosions until end of frame
        int firstExplosion = 0;
        int chainLength = 0;
        while (firstExplosion < m_explodedTilesLastFrame.Count)
        {
            int lastExplosion = m_explodedTilesLastFrame.Count;
            foreach (Bomb bomb in m_bombs)
            {
                if (bomb.posX < 0)
                    continue;

                int tileIndex = bomb.posX + bomb.posY * m_boardWidth;
                for (int i = firstExplosion; i < lastExplosion; i++)
                {
                    if (m_explodedTilesLastFrame[i] == tileIndex)
                        bomb.OnExplosion(this);
                }
            }
            firstExplosion = lastExplosion;
            chainLength ++;
        }

        m_scoreTracker.AddFrameScore(m_explodedTilesLastFrame.Count, m_currentDestroyedBlockCount, chainLength);

        m_tickNumber++;
    }

    public Tile GetTileAtPosition(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < m_boardWidth && y < m_boardHeight)
        {
            int tileIndex = x + y * m_boardWidth;
            if (m_tileList[tileIndex] != null)
                return m_tileList[tileIndex];
        }
        return new InvalidTile();
    }

    public int CountTilesOfType(Type tileType)
    {
        int count = 0;
        for (int i = 0; i < m_tileList.Length; i++)
        {
            if (m_tileList[i].GetComponent(tileType) != null)
                count++;
        }
        return count;
    }

    public int CountActiveBombs()
    {
        int count = 0;
        foreach (Bomb bomb in m_bombs)
        {
            if (bomb.posX >= 0)
                count++;
        }
        return count;
    }

    public bool IsProcessing()
    {
        if (CountActiveBombs() > 0)
            return true;

        for (int i = 0; i < m_tileList.Length; i++)
        {
            if (m_tileList[i].IsProcessing)
                return true;
        }

        return false;
    }

    public int CountTotalBombs()
    {
        return m_bombs.Count;
    }

    public void RegisterDestroyedBlock(int value)
    {
        m_currentDestroyedBlockCount++;
    }
}
