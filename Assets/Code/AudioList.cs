﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioList : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] m_audioClips = null;

    [SerializeField]
    bool m_playOnAwake = false;

    [SerializeField]
    bool m_playOnEnable = false;

    [Range(0f, 2f)]
    [SerializeField]
    float m_volume = 1.0f;

    void Awake()
    {
        if (m_playOnAwake)
            Play();
    }

    void OnEnable()
    {
        if (m_playOnEnable)
            Play();
    }

    public void Play()
    {
        if (enabled && m_audioClips != null)
        {
            int index = Random.Range(0, m_audioClips.Length);
            AudioManager.Instance.PlayAudio(m_audioClips[index], m_volume, 0f);
        }
    }
}