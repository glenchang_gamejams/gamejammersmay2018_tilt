﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance = null;

    public GameObject m_startPanel = null;
    public GameObject m_retryPanel = null;
    public GameObject m_nextPanel = null;
    public GameObject m_endPanel = null;
    public GameObject m_hudPanel = null;
    public GameObject m_scorePanel = null;
    public GameObject m_levelSelectPanel = null;
    public GameObject m_leaderBoardPanel = null;

    public Text m_retryMessageText = null;
    public Text m_retryLevelText = null;
    public Text m_nextLevelText = null;
    public InputField m_debugLoadLevel = null;
    public Text m_nameField = null;
    public Text m_levelNameText = null;


    public Text m_scoreText = null;
    public Text m_topScoreNames = null;
    public Text m_topScoreResults = null;
    public Text m_rankText = null;
    public Text m_yourScore = null;

    public Text m_yourTotalScore = null;
    public Text m_yourTotalRank = null;
    public Text m_totalScoreNames = null;
    public Text m_totalScoreResults = null;

    public int m_requestingLevelResults = 0;
    public GameObject m_levelSelectButton = null;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        EventManager.AddListener<TopScoreRecievedEvent>(OnTopScoreRecieved);
        EventManager.AddListener<RankRecievedEvent>(OnRankRecieved);
        EventManager.AddListener<StartLevelEvent>(OnStartLevel);
        EventManager.AddListener<GetLevelsScoresEvent>(OnGetLevelCompleted);
        EventManager.AddListener<TotalScoreRecievedEvent>(OnTotalScoreRecieved);
        EventManager.AddListener<TotalRankRecievedEvent>(OnTotalRankRecieved);
        EventManager.AddListener<TopTotalScoresRecievedEvent>(OnTopTotalScoreRecieved);
    }

    private void OnDestroy()
    {
        EventManager.RemoveListener<TopScoreRecievedEvent>(OnTopScoreRecieved);
        EventManager.RemoveListener<RankRecievedEvent>(OnRankRecieved);
        EventManager.RemoveListener<StartLevelEvent>(OnStartLevel);
        EventManager.RemoveListener<GetLevelsScoresEvent>(OnGetLevelCompleted);
        EventManager.RemoveListener<TotalScoreRecievedEvent>(OnTotalScoreRecieved);
        EventManager.RemoveListener<TotalRankRecievedEvent>(OnTotalRankRecieved);
        EventManager.RemoveListener<TopTotalScoresRecievedEvent>(OnTopTotalScoreRecieved);
    }

    public void Start()
    {
        InitializeLevelSelect();
        m_startPanel.SetActive(true);
        m_retryPanel.SetActive(false);
        m_nextPanel.SetActive(false);
        m_endPanel.SetActive(false);
        m_hudPanel.SetActive(false);
        m_scorePanel.SetActive(false);
        m_levelSelectPanel.SetActive(false);
        m_leaderBoardPanel.SetActive(false);
    }

    public void OnStartLevel()
    {
        m_startPanel.SetActive(false);
        m_retryPanel.SetActive(false);
        m_nextPanel.SetActive(false);
        m_endPanel.SetActive(false);
        m_hudPanel.SetActive(true);
        m_scorePanel.SetActive(false);
        m_levelSelectPanel.SetActive(false);
    }

    public void OnRetry()
    {
        OnStartLevel();
    }

    public void OnFailedLevel(int level, string message= "Better Luck Next Time")
    {
        m_retryMessageText.text = message;

        m_retryLevelText.text = string.Format("Retry Level {0}?", BoardDatabase.Instance.GetBoardReadableName(level));
        m_startPanel.SetActive(false);
        m_retryPanel.SetActive(true);
        m_nextPanel.SetActive(false);
        m_endPanel.SetActive(false);
        m_hudPanel.SetActive(false);
        m_scorePanel.SetActive(false);
        m_levelSelectPanel.SetActive(false);
    }

    public void OnLevelComplete(int level)
    {
        UpdateScore();
        m_nextLevelText.text = string.Format("Go To Level {0}?", BoardDatabase.Instance.GetBoardReadableName(level + 1));
        m_startPanel.SetActive(false);
        m_retryPanel.SetActive(false);
        m_nextPanel.SetActive(true);
        m_endPanel.SetActive(false);
        m_hudPanel.SetActive(false);
        m_scorePanel.SetActive(true);
        m_levelSelectPanel.SetActive(false);
    }

    public void OnGameComplete()
    {
        UpdateScore();
        m_startPanel.SetActive(false);
        m_retryPanel.SetActive(false);
        m_nextPanel.SetActive(false);
        m_endPanel.SetActive(true);
        m_hudPanel.SetActive(false);
        m_scorePanel.SetActive(true);
        m_levelSelectPanel.SetActive(false);
    }

    public void OnRestart()
    {
        m_startPanel.SetActive(true);
        m_retryPanel.SetActive(false);
        m_nextPanel.SetActive(false);
        m_endPanel.SetActive(false);
        m_hudPanel.SetActive(false);
        m_scorePanel.SetActive(false);
        m_levelSelectPanel.SetActive(false);
    }

    // Button callbacks
    public void SetPlayerName()
    {
        GameManager.Instance.PlayerName = string.IsNullOrEmpty(m_nameField.text) ? "Demo Man" : m_nameField.text;
    }

    public void StartGameButtonPress()
    {
        SetPlayerName();
        GameManager.Instance.StartGame();
    }

    public void RetryLevelButtonPress()
    {
        GameManager.Instance.RetryLevel();
    }

    public void NextLevelButtonPress()
    {
        GameManager.Instance.NextLevel();
    }

    public void EndGameButtonPress()
    {
        GameManager.Instance.RestartGame();
    }

    public void DebugLoadLevel()
    {
        var level = Int32.Parse(m_debugLoadLevel.text);
        GameManager.Instance.LoadLevel(level);
    }

    public void StartTurnButtonPress()
    {
        EventManager.Fire(new UIStartTurnEvent());
    }

    public void QuitLevelButtonPress()
    {
        GameManager.Instance.QuitLevel();
    }

    public void DebugPassLevelButtonPress()
    {
        GameManager.Instance.LevelCompleted();
    }

    public void LeftButtonPressed()
    {
        EventManager.Fire(new UIRotateCameraLeftEvent());
    }

    public void RightButtonPressed()
    {
        EventManager.Fire(new UIRotateCameraRightEvent());
    }

    public void LevelSelected(string level)
    {
        Debug.LogFormat("Level selected ", level);
    }


    public void ShowLevelSelectPanel()
    {
        m_startPanel.SetActive(false);
        m_retryPanel.SetActive(false);
        m_nextPanel.SetActive(false);
        m_endPanel.SetActive(false);
        m_hudPanel.SetActive(false);
        m_scorePanel.SetActive(false);
        m_levelSelectPanel.SetActive(true);

        SetPlayerName();
        LeaderBoards.Instance.GetLevelScores();
    }

    public void ShowLeaderBoardsPanel()
    {
        m_yourTotalRank.text = "";
        m_yourTotalScore.text = "";
        m_leaderBoardPanel.SetActive(true);
        LeaderBoards.Instance.ShowTopTotalScores();
    }

    protected GridLayoutGroup FindLevelSelectLayout()
    {
        if (m_levelSelectButton == null || m_levelSelectPanel == null)
            return null;

        GridLayoutGroup gridPanel = null;
        int childCount = m_levelSelectPanel.transform.childCount;
        for (int i = 0; i < childCount; i++)
        {
            gridPanel = m_levelSelectPanel.transform.GetChild(i).gameObject.GetComponent(typeof(GridLayoutGroup)) as GridLayoutGroup;
            if (gridPanel != null)
                break;
        }

        return gridPanel;
    }

    protected void UpdateLevelSelectScores()
    {
        GridLayoutGroup gridPanel = FindLevelSelectLayout();
        if (gridPanel == null)
            return;

        var levelButtons = gridPanel.GetComponentsInChildren<LevelSelectButtonScript>();
        foreach (var button in levelButtons)
        {
            Transform levelScore = button.transform.Find("LevelScore");
            UnityEngine.UI.Text scoreComponent = levelScore.gameObject.GetComponent(typeof(UnityEngine.UI.Text)) as UnityEngine.UI.Text;

            if (LeaderBoards.Instance.LevelScores.ContainsKey(button.m_levelIndex))
            {
                var score = LeaderBoards.Instance.LevelScores[button.m_levelIndex];
                scoreComponent.text = "{0}".FormatString(score);
            }
            else
                scoreComponent.text = "";
        }
    }

    protected void InitializeLevelSelect()
    {
        SetPlayerName();
        GridLayoutGroup gridPanel = FindLevelSelectLayout();
        if (gridPanel == null)
            return;

        BoardDatabase db = BoardDatabase.Instance;
        for (int i = 0; i < db.GetBoardCount(); i++)
        {
            GameObject newButton = Instantiate<GameObject>(m_levelSelectButton, gridPanel.gameObject.transform);

            // Set the level index so loading works
            LevelSelectButtonScript buttonScript = newButton.GetComponent(typeof(LevelSelectButtonScript)) as LevelSelectButtonScript;
            buttonScript.m_star.enabled = false;

            if (buttonScript != null)
                buttonScript.m_levelIndex = i;

            Transform levelName = newButton.transform.Find("LevelName");

            if (levelName != null && levelName.gameObject != null)
            {
                UnityEngine.UI.Text label = levelName.gameObject.GetComponent(typeof(UnityEngine.UI.Text)) as UnityEngine.UI.Text;
                string name = db.GetBoardReadableName(i);

                label.text = name;
            }
        }
    }

    public void UpdateScore()
    {
        var gameState = GameManager.Instance.CurrentState;

        string scoreResult = "No Results";

        if (gameState != null)
        {
            var score = gameState.m_scoreTracker.m_totalScore;
            scoreResult = string.Format(
                "Largest Explosion : {0}\n" +
                "Ignition Count : {1}\n" +
                "Blocks Destroyed : {2}\n" +
                "Most Blocks Destroyed : {3}\n" +
                "Longest Chain : {4}\n",
                gameState.m_scoreTracker.m_largestExplosion,
                gameState.m_scoreTracker.m_ignitionCount,
                gameState.m_scoreTracker.m_blocksDestroyed,
                gameState.m_scoreTracker.m_mostBlocksDestroyedInTick,
                gameState.m_scoreTracker.m_longestChain);

            m_yourScore.text = "Your Score : " + score.ToString();
            m_requestingLevelResults = GameManager.Instance.Level;
            LeaderBoards.Instance.SetScore(string.Format("{0}:{1}",
                GameManager.Instance.PlayerName,
                GameManager.Instance.Level), GameManager.Instance.Level, score);
        }
        m_scoreText.text = scoreResult;
    }
    
    private void OnTopScoreRecieved(TopScoreRecievedEvent evt)
    {
        if (evt.TopScores == null)
            return;

        m_topScoreNames.text = "";
        m_topScoreResults.text = "";

        Debug.Log(m_requestingLevelResults);

        foreach (var record in evt.TopScores)
        {
            string[] name = record.PlayerName.Split(':');
            if (name.Length != 2)
            {
                Debug.LogFormat("Bad record for {0}", record.PlayerName);
                continue;
            }

            // If score is not for the current level then skip
            if (name[1] != string.Format("{0}", m_requestingLevelResults))
                continue;

            m_topScoreNames.text += string.Format("{0}\n", name[0]);
            m_topScoreResults.text += string.Format("{0}\n", record.Score);

            m_rankText.text = "Your Rank : " + record.Rank;
        }
    }

    private void OnRankRecieved(RankRecievedEvent evt)
    {
        m_rankText.text = "Your Rank : " + evt.Rank.ToString();
    }

    public void OnStartLevel(StartLevelEvent evt)
    {
        m_levelNameText.text = string.Format("LEVEL {0}",BoardDatabase.Instance.GetBoardReadableName(evt.Level).ToUpper());
    }

    public void OnGetLevelCompleted(GetLevelsScoresEvent evt)
    {
        var buttons = m_levelSelectPanel.GetComponentsInChildren<LevelSelectButtonScript>();

        foreach (var but in buttons)
        {
            if (evt.LevelRecords.ContainsKey(but.m_levelIndex))
            {
                but.m_star.enabled = true;
            }
        }
        UpdateLevelSelectScores();
    }

    public void OnTotalScoreRecieved(TotalScoreRecievedEvent evt)
    {
        m_yourTotalScore.text = "Your Score {0}".FormatString(evt.Score);
    }

    public void OnTotalRankRecieved(TotalRankRecievedEvent evt)
    {
        m_yourTotalRank.text = "Your Rank {0}".FormatString(evt.Rank);
    }

    public void OnTopTotalScoreRecieved(TopTotalScoresRecievedEvent evt)
    {
        if (evt.TopScores == null)
            return;

        m_totalScoreNames.text = "";
        m_totalScoreResults.text = "";

        foreach (var record in evt.TopScores)
        {
            m_totalScoreNames.text += "{0} : {1}\n".FormatString(record.Rank, record.PlayerName);
            m_totalScoreResults.text += "{0}\n".FormatString(record.Score);
        }
    }
}

