﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance = null;

    public int m_maxSounds = 20;
    private AudioSource[] m_audioSources;
    private AudioSource m_music;
    public AudioClip m_startMusic = null;
    public AudioClip[] m_backgroundMusic = null;
    public bool m_playMusicOnStart = true;
    public int m_musicIndex = 0;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        List<AudioSource> audioSourceList = new List<AudioSource>();
        for (int i = 0; i < m_maxSounds; i++)
        {
            audioSourceList.Add(gameObject.AddComponent<AudioSource>());
        }
        m_audioSources = audioSourceList.ToArray();

        EventManager.AddListener<StartLevelEvent>(OnLevelStart);
    }

    private void Start()
    {
       m_music = gameObject.AddComponent<AudioSource>();
        if (m_playMusicOnStart)
        {
            if (m_startMusic != null)
                PlayMusic(m_startMusic, true);
        }
    }

    public void PlayAudio(AudioClip audioClip, float volume = 1.0f, float delayInSeconds = 0.0f)
    {
        for (int i = 0; i < m_maxSounds; i++)
        {
            if (Instance.m_audioSources[i].isPlaying)
            {
                continue;
            }

            Instance.m_audioSources[i].clip = audioClip;
            Instance.m_audioSources[i].volume = volume;
            Instance.m_audioSources[i].PlayDelayed(delayInSeconds);

            // Found a free audio source quit
            return;
        }
        Debug.LogWarningFormat("Not enough audio sources to play {0}", audioClip.name);
    }

    public void PlayMusic(AudioClip audioClip, bool loop, float delayInSeconds = 0.0f)
    {
        Instance.m_music.clip = audioClip;
        Instance.m_music.loop = loop;
        Instance.m_music.Play();
    }

    public void OnLevelStart(StartLevelEvent evt)
    {
        PlayMusic(m_backgroundMusic[m_musicIndex++ % m_backgroundMusic.Length], true, 0.5f);
    }
}
