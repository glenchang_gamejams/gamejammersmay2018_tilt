﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    public bool useStartPanel = true;
    public int Level = 0;
    public string PlayerName = "Demo";
    public GameState CurrentState = null;

    public bool m_killedGrandMa = false;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        EventManager.AddListener<TurnEndEvent>(OnTurnEnded);
        EventManager.AddListener<KilledGrandmaEvent>(OnKilledGrandma);

        // Start game immediately if useStartPanel is false
        if (!useStartPanel)
            StartGame();
    }

    private void OnDestroy()
    {
        EventManager.RemoveListener<TurnEndEvent>(OnTurnEnded);
        EventManager.RemoveListener<KilledGrandmaEvent>(OnKilledGrandma);
    }

    public void OnTurnEnded(TurnEndEvent evt)
    {
        // If we failed, we should ignore other scenarios
        if (m_killedGrandMa)
        {
            m_killedGrandMa = false;
            UIManager.Instance.OnFailedLevel(Level, "You Killed Grandma!!!");
            return;
        }

        if (evt.CurrentGameState.CountTilesOfType(typeof(CriticalBlockTile)) == 0)
        {
            LevelCompleted();
            return;
        }

        if (evt.CurrentGameState.CountTotalBombs() == 0)
        {
            UIManager.Instance.OnFailedLevel(Level, "You Ran Out Of Bombs!");
        }
    }

    private void GameCompleted()
    {
        UIManager.Instance.OnGameComplete();
        EventManager.Fire(new GameCompletedEvent());
    }

    public void StartGame()
    {
        UIManager.Instance.OnStartLevel();
        EventManager.Fire(new StartLevelEvent(Level));
    }
    public void RestartGame()
    {
        UIManager.Instance.OnRestart();
        EventManager.Fire(new RestartGameEvent());
    }

    public void LevelCompleted()
    {
        if (Level < BoardDatabase.Instance.LevelCount-1)
        {
            UIManager.Instance.OnLevelComplete(Level);
        }
        else
        {
            // Congrats!
            UIManager.Instance.OnGameComplete();
        }
    }

    public void RetryLevel()
    {
        UIManager.Instance.OnRetry();
        EventManager.Fire(new StartLevelEvent(Level));
    }

    public void OnKilledGrandma(KilledGrandmaEvent evt)
    {
        m_killedGrandMa = true;
    }

    public void NextLevel()
    {
        Level++;
        UIManager.Instance.OnStartLevel();
        EventManager.Fire(new StartLevelEvent(Level));
    }

    public void LoadLevel(int level)
    {
        Level = level;
        UIManager.Instance.OnStartLevel();
        EventManager.Fire(new StartLevelEvent(level));
    }

    public void QuitLevel()
    {
        UIManager.Instance.OnFailedLevel(Level, "Better Luck Next Time!");
    }

    public void Tick(GameState gameState)
    {
        CurrentState = gameState;
    }
}
