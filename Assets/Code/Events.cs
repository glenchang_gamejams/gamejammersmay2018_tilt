﻿using System.Collections.Generic;

public class CriticalBlockDestroyedEvent : TEventBase
{
    public Tile DestroyedTile { get; set; }
    public CriticalBlockDestroyedEvent(Tile tile)
    {
        DestroyedTile = tile;
    }
}

public class KilledGrandmaEvent : TEventBase
{
}

public class StartLevelEvent : TEventBase
{
    public int Level { get; set; }
    public StartLevelEvent(int level)
    {
        Level = level;
    }
}

public class LevelLoadedEvent : TEventBase
{
    public int Level { get; set; }
    public LevelLoadedEvent(int level)
    {
        Level = level;
    }
}

public class GameCompletedEvent : TEventBase
{
}

public class RestartGameEvent : TEventBase
{
}

public class TurnEndEvent : TEventBase
{
    public GameState CurrentGameState { get; set; }

    public TurnEndEvent(GameState gameState)
    {
        CurrentGameState = gameState;
    }
}

// UI events
public class UIStartTurnEvent : TEventBase
{
}

public class UIRotateCameraRightEvent : TEventBase
{

}

public class UIRotateCameraLeftEvent : TEventBase
{

}

public class ScoreChangedEvent : TEventBase
{
    public int Score { get; set; }

    public ScoreChangedEvent(int score)
    {
        Score = score;
    }
}

public class RankRecievedEvent : TEventBase
{
    public int Rank { get; set; }

    public RankRecievedEvent(int rank)
    {
        Rank = rank;
    }
}

public class TotalRankRecievedEvent : TEventBase
{
    public int Rank { get; set; }

    public TotalRankRecievedEvent(int rank)
    {
        Rank = rank;
    }
}

public class TotalScoreRecievedEvent : TEventBase
{
    public int Score { get; set; }

    public TotalScoreRecievedEvent(int score)
    {
        Score = score;
    }
}

public class TopScoreRecievedEvent : TEventBase
{
    public List<ScoreRecord> TopScores { get; set; }

    public TopScoreRecievedEvent(List<ScoreRecord> topScores)
    {
        TopScores = topScores;
    }
}

public class TopTotalScoresRecievedEvent : TEventBase
{
    public List<TotalScoreRecord> TopScores { get; set; }

    public TopTotalScoresRecievedEvent(List<TotalScoreRecord> topScores)
    {
        TopScores = topScores;
    }
}

public class GetLevelsScoresEvent : TEventBase
{
    public Dictionary<int, int> LevelRecords { get; set; }

    public GetLevelsScoresEvent(Dictionary<int, int> levelRecords)
    {
        LevelRecords = levelRecords;
    }
}

public class PlayerNameChangedEvent : TEventBase
{
    public string PlayerName { get; set; }

    public PlayerNameChangedEvent(string playerName)
    {
        PlayerName = playerName;
    }
}

public class ToolTipEvent : TEventBase
{
    public string Message { get; set; }

    public ToolTipEvent(string message)
    {
        Message = message;
    }
}

public class ToolTipOffEvent : TEventBase
{
}