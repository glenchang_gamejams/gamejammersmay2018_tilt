﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriticalBlockTile: Tile
{
    override public void OnExplosion(GameState caller)
    {
        base.OnExplosion(caller);
        // Fire event that this block is destroyed
        EventManager.Fire(new CriticalBlockDestroyedEvent(this));
    }
}
