﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class Tile : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int x = -1;
    public int y = -1;
    public bool IsInvalid = false;
    public bool IsBlockingExplosion = false;
    public bool CanHoldBomb = false;
    public bool CanBeDamaged = false;
    public bool IsProcessing = false;
    public string ToolTipText = "";

    public Color m_debugColor = Color.black;
    protected Health m_health = null;

    protected bool IsDestroyed = false;
    protected AudioList m_audioList = null;

    protected TextOverlay m_textOverlay = null;

    virtual protected int GetScore ()
    {
        return 1;
    }

    virtual public void Tick(GameState caller)
    {
        if (IsDestroyed)
        {
            IsProcessing = false;

            // swap this tile with a placeable bomb tile
            var placeable = TileFactory.Instance.CreateReplacementOpenTile(x, y);
            caller.ReplaceTile(x, y, placeable);
        }
    }

    virtual public void OnExplosion(GameState caller)
    {
        // Get audiolist
        m_audioList = GetComponent<AudioList>();
        if (m_audioList != null)
            m_audioList.Play();

        // If can be damaged, take damage
        // Override the behaviour when dead via m_health.IsDead()
        m_health = GetComponent<Health>();
        if (CanBeDamaged && m_health != null)
        {
            m_health.Damage(1);

            if (m_textOverlay)
            {
                m_textOverlay.SetText(m_health.CurrentHealth.ToString());
                if (m_health.CurrentHealth <= 1)
                    m_textOverlay.gameObject.SetActive(false);
            }
            if (m_health.IsDead)
            {
                if (!IsDestroyed)
                    caller.RegisterDestroyedBlock(1);
                IsProcessing = true;
                IsDestroyed = true;
            }
        }
    }

    virtual public Color GetDebugColor()
    {
        return m_debugColor;
    }

    public void AddHealthComponent(int amount)
    {
        m_health = gameObject.AddComponent<Health>();
        if (m_health == null)
            return;
        else
        {
            m_health.SetMaxHealth(amount);

            if (m_health.CurrentHealth > 1)
            {
                m_textOverlay = GetComponentInChildren<TextOverlay>();
                if (m_textOverlay != null)
                {
                    m_textOverlay.gameObject.SetActive(true);
                    m_textOverlay.SetText(m_health.CurrentHealth.ToString());
                }
            }
         }
    }

    protected void OnBombPlaced()
    {
        if (CanHoldBomb)
        {
            IsBlockingExplosion = true;
            CanHoldBomb = false;
            CanBeDamaged = true;
        }
    }

    virtual protected void Init()
    {
    }

    private void Start()
    {
        // Tint for debug
        //Renderer[] renderers = GetComponentsInChildren<Renderer>();
        //foreach (var ren in renderers)
        //    {
        //        foreach (var mat in ren.materials)
        //            mat.color = m_debugColor;
        //    }
        Init();
    }

    public void OnPointerEnter(PointerEventData evt)
    {
        EventManager.Fire(new ToolTipEvent(ToolTipText));
    }

    public void OnPointerExit(PointerEventData evt)
    {
        EventManager.Fire(new ToolTipEvent(ToolTipText));
        EventManager.Fire(new ToolTipOffEvent());
    }
}
