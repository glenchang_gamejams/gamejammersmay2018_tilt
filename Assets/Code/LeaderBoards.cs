/*
Refer to https://gamedevelopment.tutsplus.com/tutorials/how-to-code-a-self-hosted-phpsql-leaderboard-for-your-game--gamedev-11627

// Creating the table
CREATE TABLE Scores(
    name VARCHAR(10) NOT NULL DEFAULT 'Anonymous' PRIMARY KEY,
    score INT(5) UNSIGNED NOT NULL DEFAULT '0',
    ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)
ENGINE=InnoDB; 
 */


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct ScoreRecord
{
    public string PlayerName;
    public string Score;
    public string Rank;

    public ScoreRecord(string playerName, string score, string rank)
    {
        PlayerName = playerName;
        Score = score;
        Rank = rank;
    }
}

public struct TotalScoreRecord
{
    public string PlayerName;
    public string Score;
    public string Rank;

    public TotalScoreRecord(string playerName, string score, string rank)
    {
        PlayerName = playerName;
        Score = score;
        Rank = rank;
    }
}


public class LeaderBoards : MonoBehaviour
{
    public static LeaderBoards Instance = null;

    /// Fill in your server data here.
    private string m_database = "gchang_game_demoman";
    private string m_privateKey = "gbc052510180109";

    public Dictionary<int, int> LevelScores = new Dictionary<int, int>();

#if UNITY_WEBGL
    // On the web you can use the relative path for faster response
    // Don't forget the question marks!
    private string m_topScoresURL = "TopScores.php?";
    private string m_addScoreURL = "AddScore.php?";
    private string m_rankURL = "GetRank.php?";
    private string m_levelsCompleted = "LevelsCompleted.php?";

    private string m_totalTopScoresURL = "TotalScores.php?";
    private string m_totalAddScoreURL = "AddTotalScore.php?";
    private string m_totalRankURL = "GetTotalRank.php?"; 
#else
    // On anything else just use the url
    // Don't forget the question marks!
    private string m_topScoresURL = "http://www.glenchang.com/main/games/DemoMan/TopScores.php?";
    private string m_addScoreURL = "http://www.glenchang.com/main/games/DemoMan/AddScore.php?";
    private string m_rankURL = "http://www.glenchang.com/main/games/DemoMan/GetRank.php?"; 
    private string m_levelsCompleted = "http://www.glenchang.com/main/games/DemoMan/LevelsCompleted.php?";

    private string m_totalTopScoresURL = "http://www.glenchang.com/main/games/DemoMan/TotalScores.php?";
    private string m_totalAddScoreURL = "http://www.glenchang.com/main/games/DemoMan/AddTotalScore.php?";
    private string m_totalRankURL = "http://www.glenchang.com/main/games/DemoMan/GetTotalRank.php?"; 
#endif

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

#region Level Scores
    public void SetScore(string playerName, int level, int score)
    {
        StartCoroutine(AddScore(playerName, level, score));
    }

    ///Our encryption function: http://wiki.unity3d.com/index.php?title=MD5
    private string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    private IEnumerator AddScore(string playerName, int level, int score)
    {
        string hash = Md5Sum(playerName + score + m_privateKey);
        //Debug.Log(m_addScoreURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(playerName), "&level=", level, "&score=", score, "&hash=", hash));
        WWW ScorePost = new WWW(m_addScoreURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(playerName), "&level=", level, "&score=", score, "&hash=", hash));
        yield return ScorePost; // The function halts until the score is posted.
        if (ScorePost.error == null)
        {
            Debug.Log("Success in sending add score");

            StartCoroutine(GetTopScores(UIManager.Instance.m_requestingLevelResults)); // Get our top scores
            //StartCoroutine(GrabRank( GameManager.Instance.PlayerName, UIManager.Instance.m_requestingLevelResults));

            // Also determine and set total score
            StartCoroutine(DetermineTotalScoreRoutine());
        }
        else
        {
            Debug.Log("Failed to add score");
        }
    }

    IEnumerator GrabRank(string playerName, int level)
    {
        //Try and grab the Rank
        //Debug.Log(m_rankURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(playerName), "&level=", level));
        WWW RankGrabAttempt = new WWW(m_rankURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(playerName)));

        yield return RankGrabAttempt;

        if (RankGrabAttempt.error == null)
        {
            //Debug.LogFormat("Recieved Rank {0}", RankGrabAttempt.text);
            int rank;
            if(System.Int32.TryParse(RankGrabAttempt.text, out rank))
                EventManager.Fire(new RankRecievedEvent(rank));
        }
        else
        {
            Debug.Log("Failed to Get Rank");
        }
    }

    IEnumerator GetTopScores(int level)
    {
        yield return new WaitForSeconds(0.1f);
        WWW GetScoresAttempt = new WWW(m_topScoresURL.ConcatString("database=", m_database, "&level=", level));
        yield return GetScoresAttempt;

        if (GetScoresAttempt.error != null)
        {
            Debug.Log("Failed to get top scores");
        }
        else
        {
            //Debug.LogFormat("Recieved Top Scores {0}", GetScoresAttempt.text);

            //Collect up all our data
            string[] textlist = GetScoresAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);

            //Split it into two smaller arrays
            string[] Names = new string[Mathf.FloorToInt(textlist.Length / 2)];
            string[] Scores = new string[Names.Length];
            for (int i = 0; i < textlist.Length; i++)
            {
                if (i % 2 == 0)
                    Names[Mathf.FloorToInt(i / 2)] = textlist[i];
                else
                    Scores[Mathf.FloorToInt(i / 2)] = textlist[i];
                //Debug.Log(textlist[i].ToString());
            }

            List<ScoreRecord> scoreRecords = new List<ScoreRecord>();
            for (int i = 0; i < Names.Length; i++)
            {
                scoreRecords.Add(new ScoreRecord(Names[i], Scores[i], (i + 1).ToString()));
            }

            EventManager.Fire(new TopScoreRecievedEvent(scoreRecords));
        }
    }
    #endregion

#region Total Scores


    ///Our IEnumerators
    IEnumerator AddTotalScore(string playerName, int score)
    {
        string hash = Md5Sum(playerName + score + m_privateKey);
        WWW ScorePost = new WWW(m_totalAddScoreURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(playerName), "&score=", score, "&hash=", hash));
        yield return ScorePost; // The function halts until the score is posted.

        if (ScorePost.error == null)
        {
            Debug.Log("Success in sending total score");
        }
        else
        {
            Debug.Log("Failed to add score");
        }
    }

    IEnumerator GrabTotalRank(string playerName)
    {
        Debug.LogFormat("Gettting rank for player {0}", playerName);
        //Try and grab the Rank
        WWW RankGrabAttempt = new WWW(m_totalRankURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(playerName)));
        yield return RankGrabAttempt;

        if (RankGrabAttempt.error == null)
        {
            //Debug.LogFormat("Recieved Rank {0}", RankGrabAttempt.text);
            int rank;
            if (System.Int32.TryParse(RankGrabAttempt.text, out rank))
                EventManager.Fire(new TotalRankRecievedEvent(rank));
        }
        else
        {
            Debug.Log("Failed to Get Rank");
        }
    }

    public void ShowTopTotalScores()
    {
        StartCoroutine(DetermineTotalScoreRoutine());
        StartCoroutine(GrabTotalRank(GameManager.Instance.PlayerName));
        StartCoroutine(GetTopTotalScores()); // Get our top scores
    }

    // This routine is called when we need to fetch the top player scores
    IEnumerator GetTopTotalScores()
    {
        yield return new WaitForSeconds(0.3f);
        Debug.Log(m_totalTopScoresURL.ConcatString("database=", m_database));
        WWW GetScoresAttempt = new WWW(m_totalTopScoresURL.ConcatString("database=", m_database));
        yield return GetScoresAttempt;

        if (GetScoresAttempt.error != null)
        {
            Debug.Log("Error in GetTopTotalScores");
        }
        else
        {
            //Collect up all our data
            string[] textlist = GetScoresAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);

            //Split it into two smaller arrays
            string[] Names = new string[Mathf.FloorToInt(textlist.Length / 2)];
            string[] Scores = new string[Names.Length];
            for (int i = 0; i < textlist.Length; i++)
            {
                if (i % 2 == 0)
                {
                    Names[Mathf.FloorToInt(i / 2)] = textlist[i];
                }
                else Scores[Mathf.FloorToInt(i / 2)] = textlist[i];
            }

            List<TotalScoreRecord> scoreRecords = new List<TotalScoreRecord>();
            for (int i = 0; i < Names.Length; i++)
            {
                scoreRecords.Add(new TotalScoreRecord(Names[i], Scores[i], (i + 1).ToString()));
            }

            EventManager.Fire(new TopTotalScoresRecievedEvent(scoreRecords));
        }
    }


    IEnumerator DetermineTotalScoreRoutine()
    {
        var playerName = GameManager.Instance.PlayerName;

        yield return new WaitForSeconds(0.1f);
        Debug.Log(m_levelsCompleted.ConcatString("database=", m_database, "&name=", playerName));
        WWW GetScoresAttempt = new WWW(m_levelsCompleted.ConcatString("database=", m_database, "&name=", playerName));
        yield return GetScoresAttempt;

        if (GetScoresAttempt.error != null)
        {
            Debug.Log("Failed to get levels passed");
        }
        else
        {
            Debug.LogFormat("Recieved Levels Passed {0}", GetScoresAttempt.text);

            //Collect up all our data
            string[] textlist = GetScoresAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);
            //Split it into two smaller arrays
            int[] Levels = new int[Mathf.FloorToInt(textlist.Length / 2)];
            int[] Scores = new int[Levels.Length];
            for (int i = 0; i < textlist.Length; i++)
            {
                if (i % 2 == 0)
                    Levels[Mathf.FloorToInt(i / 2)] = System.Int32.Parse(textlist[i]);
                else
                    Scores[Mathf.FloorToInt(i / 2)] = System.Int32.Parse(textlist[i]);
            }

            int totalScore = 0;
            for (int i = 0; i < Levels.Length; i++)
            {
                LevelScores[Levels[i]] = Scores[i];
                totalScore += Scores[i];
            }

            // Add total score to total score table
            StartCoroutine(AddTotalScore(playerName, totalScore));

            EventManager.Fire(new TotalScoreRecievedEvent(totalScore));
        }
    }

    #endregion

    IEnumerator GetLevelScoresRoutine()
    {
        var playerName = GameManager.Instance.PlayerName;

        yield return new WaitForSeconds(0.1f);
        Debug.Log(m_levelsCompleted.ConcatString("database=", m_database, "&name=", playerName));
        WWW GetLevelsAttempt = new WWW(m_levelsCompleted.ConcatString("database=", m_database, "&name=", playerName));
        yield return GetLevelsAttempt;

        if (GetLevelsAttempt.error != null)
        {
            Debug.Log("Failed to get levels passed");
        }
        else
        {
            Debug.LogFormat("Recieved Levels Passed {0}", GetLevelsAttempt.text);


            //Collect up all our data
            string[] textlist = GetLevelsAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);
            //Split it into two smaller arrays
            int[] Levels = new int[Mathf.FloorToInt(textlist.Length / 2)];
            int[] Scores = new int[Levels.Length];
            for (int i = 0; i < textlist.Length; i++)
            {
                if (i % 2 == 0)
                    Levels[Mathf.FloorToInt(i / 2)] = System.Int32.Parse(textlist[i]);
                else
                    Scores[Mathf.FloorToInt(i / 2)] = System.Int32.Parse(textlist[i]);
            }

            int totalScore = 0;
            for (int i = 0; i < Levels.Length; i++)
            {
                LevelScores[Levels[i]] = Scores[i];
                totalScore += Scores[i];
            }

            EventManager.Fire(new GetLevelsScoresEvent(LevelScores));
        }
    }

    public void GetLevelScores()
    {
        StartCoroutine(GetLevelScoresRoutine());
    }

    public void TestAddScore()
    {
        StartCoroutine(AddScore("Tester:1", 1, Random.Range(0, 100)));
    }

    public void TestGetScores()
    {
        StartCoroutine(GetTopScores(1));
    }

    public void TestRank()
    {
        StartCoroutine(GrabRank("Tester:1",1));
    }
}
