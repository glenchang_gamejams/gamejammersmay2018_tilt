﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBlockTile : Tile
{
    private bool isExploding = false;
    private int explodeTick = -1;

    override public void Tick(GameState caller)
    {
        if (!isExploding && caller.GetTickNumber() == explodeTick)
        {
            //Adding sound of bomb exploding
            // Get audiolist
            m_audioList = GetComponent<AudioList>();
            if (m_audioList != null)
                m_audioList.Play();

            //This will let the bomb know to not explode more than once
            isExploding = true;

            //Explode surrounding tiles starting with top left, moving clockwise
            caller.ExplodeTile(x - 1, y - 1, gameObject);
            caller.ExplodeTile(x, y - 1, gameObject);
            caller.ExplodeTile(x + 1, y - 1, gameObject);
            caller.ExplodeTile(x + 1, y, gameObject);
            caller.ExplodeTile(x + 1, y + 1, gameObject);
            caller.ExplodeTile(x, y + 1, gameObject);
            caller.ExplodeTile(x - 1, y + 1, gameObject);
            caller.ExplodeTile(x - 1, y, gameObject);

            base.Tick(caller);
        }
    }

    override public void OnExplosion(GameState caller)
    {
        base.OnExplosion(caller);
        if (explodeTick == -1)
        {
            explodeTick = caller.GetTickNumber() + 1;
        }
    }
}
