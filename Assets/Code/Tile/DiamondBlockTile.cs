﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondBlockTile : Tile
{
    virtual protected int GetScore()
    {
        return 10;
    }
}
