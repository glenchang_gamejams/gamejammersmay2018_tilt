﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BombDropper : MonoBehaviour, IPointerDownHandler {

    public Bomb.BombType m_type = Bomb.BombType.Surround;
    protected GameState m_gameState;
    protected VisualSceneController m_visualController;

	// Use this for initialization
	void Start () {
        m_gameState = Object.FindObjectOfType(typeof(GameState)) as GameState;
        m_visualController = Object.FindObjectOfType(typeof(VisualSceneController)) as VisualSceneController;
	}

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (m_gameState == null || m_visualController == null)
            return;

        foreach (Bomb bomb in m_gameState.m_bombs)
        {
            if (bomb.posX < 0 && bomb.bombType == m_type)
            {
                m_visualController.StartBombDrag(bomb);
                return;
            }
        }
    }
}
