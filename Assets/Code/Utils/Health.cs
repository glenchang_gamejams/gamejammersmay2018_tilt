﻿using UnityEngine;
using System.Collections;
using System;

public class Health : MonoBehaviour
{
    [SerializeField]
    private float m_maxHealth = 100f;
    public float MaxHealth { get { return m_maxHealth; } set { } }

    [SerializeField]
    private float m_currentHealth;
    public float CurrentHealth { get { return m_currentHealth; } private set { m_currentHealth = value; } }

    public bool IsDead
    {
        get
        {
            return CurrentHealth <= 0;
        }
    }

    public void SetMaxHealth(float newMaxHealth)
    {
        if (newMaxHealth <= 0)
        {
            return;
        }

        m_maxHealth = newMaxHealth;
        m_currentHealth = newMaxHealth;
    }

    public void Heal(float healAmount)
    {
        if (healAmount <= 0f)
        {
            return;
        }

        if (IsDead)
        {
            return;
        }

        CurrentHealth += healAmount;
        CurrentHealth = Mathf.Min(CurrentHealth, m_maxHealth);
    }

    public void Damage(float damageAmount)
    {
        if (damageAmount <= 0)
        {
            return;
        }

        if (IsDead)
        {
           return;
        }

        CurrentHealth = Mathf.Max(0, CurrentHealth - damageAmount);
    }
}
