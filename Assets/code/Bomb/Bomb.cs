﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class Bomb : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    //Enumerates Bomb Types
    public enum BombType { Surround, Cross, X, Push};
    public BombType bombType = BombType.Surround;

    //The number of ticks before the bomb explodes.
    //Set to 0 for no fuse (never explodes unless in chain reaction).
    private int m_fuse = 1;
    public int fuse
    {
        get
        {
            return m_fuse;
        }
        set
        {
            var textOverlay = GetComponentInChildren<TextOverlay>();
            if (textOverlay)
            {
                textOverlay.SetText((value > 0) ? value.ToString() : "");
            }
            m_fuse = value;
        }
    }

    //Put in for Anders
    public Color debugColor = Color.green;

    //Holds the position on the grid for this bomb.
    public int posX = 0;
    public int posY = 0;

    // Tutorial text
    public string ToolTipText = "";

    //Hold the sound of the bomb exploding
    private AudioList audioList = null;

    //If a Cross/X Bomb is exploding, this is true:
    private bool isExploding = false;

    //If a Cross Bomb is blocked, these are true:
    private bool blockedAbove = false;
    private bool blockedRight = false;
    private bool blockedBelow = false;
    private bool blockedLeft = false;

    //If an X Bomb is blocked, these are true:
    private bool blockedALeft = false;
    private bool blockedARight = false;
    private bool blockedBRight = false;
    private bool blockedBLeft = false;

    //If the bomb is destroyed, this is true:
    private bool isDestroyed = false;

    //This variable keeps track of the timing of the explosion
    private int explodeTick = -1;

    //Processes all the events for this bomb every game tick
    public void Tick(GameState gameState)
    {
        var textOverlay = GetComponentInChildren<TextOverlay>();
        if (textOverlay)
            textOverlay.SetText("");
        //First, we lower the fuse and ask if the bomb explodes
        m_fuse--;
        if (m_fuse == 0)
            OnExplosion(gameState);

        //If this is a cross bomb in the middle of exploding, then
        //target the tiles two squares away.
        if (bombType == BombType.Cross && isExploding && gameState.GetTickNumber() == explodeTick)
        {
            if (!blockedAbove)
                gameState.ExplodeTile(posX, posY + 2, gameObject);
            if (!blockedRight)
                gameState.ExplodeTile(posX + 2, posY, gameObject);
            if (!blockedBelow)
                gameState.ExplodeTile(posX, posY - 2, gameObject);
            if (!blockedLeft)
                gameState.ExplodeTile(posX - 2, posY, gameObject);

            //The bomb is subsequently destroyed
            isDestroyed = true;
        }

        //If this is an X bomb in the middle of exploding, then
        //target the tiles two squares away.
        if (bombType == BombType.X && isExploding && gameState.GetTickNumber() == explodeTick)
        {
            if (!blockedALeft)
                gameState.ExplodeTile(posX - 2, posY + 2, gameObject);
            if (!blockedARight)
                gameState.ExplodeTile(posX + 2, posY + 2, gameObject);
            if (!blockedBRight)
                gameState.ExplodeTile(posX + 2, posY - 2, gameObject);
            if (!blockedBLeft)
                gameState.ExplodeTile(posX - 2, posY - 2, gameObject);

            //The bomb is subsequently destroyed
            isDestroyed = true;
        }
    }

    //Processes all the events which happen when this bomb explodes
    public void OnExplosion(GameState gameState)
    {
        //Note: We also may want to change the rendering of the bomb to show that it is exploding

        //Adding sound of bomb exploding
        audioList = gameObject.GetComponent<AudioList>();
        if (audioList != null)
            audioList.Play();

        //If this is a "Surround" bomb type:
        if (bombType == BombType.Surround && !isExploding)
        {
            //This will let the bomb know to not explode more than once
            isExploding = true;

            //Explode surrounding tiles starting with top left, moving clockwise
            gameState.ExplodeTile(posX - 1, posY + 1, gameObject);
            gameState.ExplodeTile(posX, posY + 1, gameObject);
            gameState.ExplodeTile(posX + 1, posY + 1, gameObject);
            gameState.ExplodeTile(posX + 1, posY, gameObject);
            gameState.ExplodeTile(posX + 1, posY - 1, gameObject);
            gameState.ExplodeTile(posX, posY - 1, gameObject);
            gameState.ExplodeTile(posX - 1, posY - 1, gameObject);
            gameState.ExplodeTile(posX - 1, posY, gameObject);

            //The bomb is subsequently destroyed
            isDestroyed = true;
        }
        //If this is a "Cross" bomb type and it is not already exploding:
        else if (bombType == BombType.Cross && !isExploding)
        {
            //This will let the bomb know to explode two tiles away on the next Tick
            isExploding = true;

            //Check the surrounding tiles to see if any tile is blocking an explosion
            if (gameState.IsBlockedTile(posX, posY + 1, this.gameObject))
                blockedAbove = true;
            if (gameState.IsBlockedTile(posX + 1, posY, this.gameObject))
                blockedRight = true;
            if (gameState.IsBlockedTile(posX, posY - 1, this.gameObject))
                blockedBelow = true;
            if (gameState.IsBlockedTile(posX - 1, posY, this.gameObject))
                blockedLeft = true;

            //Explode adjacent tiles.
            //Then on the next tick, explode the tiles two spaces away. (See Tick function)
            gameState.ExplodeTile(posX, posY + 1, gameObject);
            gameState.ExplodeTile(posX + 1, posY, gameObject);
            gameState.ExplodeTile(posX, posY - 1, gameObject);
            gameState.ExplodeTile(posX - 1, posY, gameObject);

            //Change the explode tick to the next game tick
            explodeTick = gameState.GetTickNumber() + 1;
        }
        //If this is an "X" bomb type and it is not already exploding:
        else if (bombType == BombType.X && !isExploding)
        {
            //This will let the bomb know to explode two tiles away on the next Tick
            isExploding = true;

            //Check the surrounding tiles to see if any tile is blocking an explosion
            if (gameState.IsBlockedTile(posX - 1, posY + 1, this.gameObject))
                blockedALeft = true;
            if (gameState.IsBlockedTile(posX + 1, posY + 1, this.gameObject))
                blockedARight = true;
            if (gameState.IsBlockedTile(posX - 1, posY - 1, this.gameObject))
                blockedBLeft = true;
            if (gameState.IsBlockedTile(posX + 1, posY - 1, this.gameObject))
                blockedBRight = true;

            //Explode adjacent tiles.
            //Then on the next tick, explode the tiles two spaces away. (See Tick function)
            gameState.ExplodeTile(posX - 1, posY + 1, gameObject);
            gameState.ExplodeTile(posX + 1, posY + 1, gameObject);
            gameState.ExplodeTile(posX + 1, posY - 1, gameObject);
            gameState.ExplodeTile(posX - 1, posY - 1, gameObject);
            
            //Change the explode tick to the next game tick
            explodeTick = gameState.GetTickNumber() + 1;
        }

        //If this is a "Push" bomb type:
        if (bombType == BombType.Push && !isExploding)
        {
            //This will let the bomb know to not explode more than once
            isExploding = true;

            //Push adjacent tiles
            gameState.PushObject(posX, posY + 1, posX, posY + 2, gameObject);
            gameState.PushObject(posX + 1, posY, posX + 2, posY, gameObject);
            gameState.PushObject(posX, posY - 1, posX, posY - 2, gameObject);
            gameState.PushObject(posX - 1, posY, posX - 2, posY, gameObject);

            //The bomb is subsequently destroyed
            isDestroyed = true;
        }
    }

    //Processes the blocking of a bomb explosion. Only matters with Cross/X Bombs
    public bool IsBlockingBomb()
    {
        return true;
    }

    //Processes the destruction of this bomb
    public bool IsDestroyed()
    {
        return isDestroyed;
    }

    public void OnPointerEnter(PointerEventData evt)
    {
        EventManager.Fire(new ToolTipEvent(ToolTipText));
    }

    public void OnPointerExit(PointerEventData evt)
    {
        EventManager.Fire(new ToolTipEvent(ToolTipText));
        EventManager.Fire(new ToolTipOffEvent());
    }
}
