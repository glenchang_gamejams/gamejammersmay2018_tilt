﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInSec : MonoBehaviour
{
    public float m_inSeconds = 1f;
	// Use this for initialization
	void Start ()
    {
        Destroy(gameObject, 1f);
	}
}
